//
//  AppDelegate.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 2/25/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Firebase
import Paystack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000.0, vertical: 0.0), for: .default)
        
        Thread.sleep(forTimeInterval: TimeInterval(Constants.appLoadingTime))
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.backgroundColor = .white
        
        let welcomeVc = WelcomePageViewController()
        let status = UserDefaults.standard.bool(forKey: StringConstants.status)
        
        if status {
            ViewSwitcher.updateRootVC()
        } else {
            window?.rootViewController = welcomeVc //Set our root view to the welcome page
        }
        
        Paystack.setDefaultPublicKey("pk_live_908dd8fd441d0cb3a2dbc81b66c1e7e8b327272b")
        
        return true
    }
}
