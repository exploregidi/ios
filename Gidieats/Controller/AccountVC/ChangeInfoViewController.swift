//
//  ChangeInfoViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 4/4/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class ChangeInfoViewController: UIViewController, UITextFieldDelegate {
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.clipsToBounds = true
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.black]

        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = titleColor
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
    
    let passwordField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.isSecureTextEntry = true
        field.returnKeyType = .next
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let newPasswordField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.newPassword, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.isSecureTextEntry = true
        field.returnKeyType = .go
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let toggleButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("SHOW", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.backgroundColor = ColorConstants.gidiGray.cgColor
        button.layer.cornerRadius = Constants.texFieldHeightOffset
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        button.addTarget(self, action: #selector(showPassword), for: .touchUpInside)
        return button
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {
        view.backgroundColor = .white
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .black
        
        navigationItem.title = StringConstants.info
        
        passwordField.delegate = self
        newPasswordField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(passwordField)
        view.addSubview(newPasswordField)
        view.addSubview(toggleButton)
        view.addSubview(submitButton)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = passwordField.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = newPasswordField.anchor(passwordField.bottomAnchor, left: passwordField.leadingAnchor, right: passwordField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, rightConstant: Constants.buttonFormWidth, heightConstant: Constants.textFieldHeight)
        _ = toggleButton.anchor(left: newPasswordField.trailingAnchor, centerY: newPasswordField.centerYAnchor, widthConstant: Constants.buttonFormWidth)
        _ = submitButton.anchor(newPasswordField.bottomAnchor, left: newPasswordField.leadingAnchor, right: passwordField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
    }
    
    var buttonClick = false
    
    @objc func showPassword() {
        buttonClick = !buttonClick
        if buttonClick == false {
            toggleButton.setTitle("SHOW", for: .normal)
            passwordField.isSecureTextEntry = true
            newPasswordField.isSecureTextEntry = true
        } else {
            toggleButton.setTitle("HIDE", for: .normal)
            passwordField.isSecureTextEntry = false
            newPasswordField.isSecureTextEntry = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordField {
            newPasswordField.becomeFirstResponder()
        } else {
            submitForm()
        }
        return true
    }
    
    @objc func submitForm() {
        self.view.endEditing(true)
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let password = passwordField.text
        let newPassword = newPasswordField.text
        
        if (password!.isEmpty) {
            passwordField.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (newPassword!.isEmpty) {
            newPasswordField.attributedPlaceholder = NSAttributedString(string: StringConstants.newPassword, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (newPassword?.isValidPassword() == false) {
            self.alert(message: StringConstants.shortPassword, title: StringConstants.error)
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                "currentPassword": password!,
                "newPassword": newPassword!
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + token
            ]
            
            AF.request(APIConstants.baseURL + APIConstants.account + "password/change", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() { //Check connectivity status
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                let userData = info[APIConstants.data] as? String
                                
                                self.alertAndPop(message: userData ?? APIConstants.changedPassword, title: StringConstants.success)
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure (_):
                        let response = response.debugDescription
                        if response.contains(APIConstants.unauthorizedResponse) {
                            newToken()
                            
                            let time = DispatchTime.now() + Constants.tokenTimer
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                self.submitForm()
                            }
                        } else if response.contains(APIConstants.badResponse) {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                        } else {
                            DispatchQueue.main.async {
                                self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                            }
                        }
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
}
