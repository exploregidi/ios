//
//  DeleteAccountViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 4/4/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class DeleteAccountViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    let warningLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        label.text = "We're sorry to see you leave. Are you sure you want to delete your account? You'll have 7 days to sign back in before it is fully deleted. This action cannot be undone once your account is fully deleted."
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let deleteButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.deleteAccount, for: .normal)
        button.backgroundColor = ColorConstants.gidiGreen
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.layer.cornerRadius = Constants.indicatorValue
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(deleteAccount), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {
        view.backgroundColor = .white
        
        navigationItem.title = StringConstants.deleteAccount
        
        navigationController?.navigationBar.shadowImage = UIImage()
        
        view.addSubview(warningLabel)
        view.addSubview(deleteButton)
        
        _ = warningLabel.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = deleteButton.anchor(warningLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, leftConstant: Constants.buttonSideConstraint, rightConstant: Constants.buttonSideConstraint, heightConstant: Constants.smallButtonSize)
    }
    
    @objc func deleteAccount() {
        let alertController = UIAlertController(title: "Confirm delete", message: "Don't leave meee 😫", preferredStyle: .alert)
        let continueButton = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            
            let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
            let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
            
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + token
            ]
            
            AF.request(APIConstants.baseURL + APIConstants.users + userId, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() { //Check connectivity status
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                                UserDefaults.standard.set(StringConstants.defaultFirstName, forKey: APIConstants.firstName)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.emailAddress)
                                
                                self.switchView(value: false)
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        let response = response.debugDescription
                        if response.contains(APIConstants.unauthorizedResponse) {
                            newToken()
                            
                            let time = DispatchTime.now() + Constants.tokenTimer
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                self.deleteAccount()
                            }
                        } else if response.contains(APIConstants.badResponse) {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                        } else {
                            DispatchQueue.main.async {
                                self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                            }
                        }
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
        
        let cancelButton = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertController.addAction(continueButton)
        alertController.addAction(cancelButton)
        alertController.view.tintColor = ColorConstants.gidiGreen
        presentViewController(viewController: alertController)
    }
}
