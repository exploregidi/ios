//
//  LoginViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setEmail() //Reset to most current email for the textfield
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
    
    let emailField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .emailAddress
        field.autocorrectionType = .no
        field.returnKeyType = .next
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let passwordField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.isSecureTextEntry = true
        field.returnKeyType = .go
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let toggleButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("SHOW", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.backgroundColor = ColorConstants.gidiGray.cgColor
        button.layer.cornerRadius = Constants.texFieldHeightOffset
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        button.addTarget(self, action: #selector(showPassword), for: .touchUpInside)
        return button
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    let forgotPasswordButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.forgotPassword, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.addTarget(self, action: #selector(forgotPassword), for: .touchUpInside)
        return button
    }()
    
    let registerButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.register, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.addTarget(self, action: #selector(register), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {
        view.backgroundColor = .white
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = ColorConstants.gidiGreen
        
        navigationItem.title = "Log in to Gidi Eats"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(closePage))
        
        emailField.delegate = self
        passwordField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(emailField)
        view.addSubview(passwordField)
        view.addSubview(toggleButton)
        view.addSubview(submitButton)
        view.addSubview(forgotPasswordButton)
        view.addSubview(registerButton)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = emailField.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = passwordField.anchor(emailField.bottomAnchor, left: emailField.leadingAnchor, right: emailField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, rightConstant: Constants.buttonFormWidth, heightConstant: Constants.textFieldHeight)
        _ = toggleButton.anchor(left: passwordField.trailingAnchor, centerY: passwordField.centerYAnchor, widthConstant: Constants.buttonFormWidth)
        _ = submitButton.anchor(passwordField.bottomAnchor, left: passwordField.leadingAnchor, right: emailField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
        _ = forgotPasswordButton.anchor(submitButton.bottomAnchor, centerX: submitButton.centerXAnchor, topConstant: Constants.minConstraintOffset)
        _ = registerButton.anchor(forgotPasswordButton.bottomAnchor, centerX: forgotPasswordButton.centerXAnchor)
    }
    
    @objc func closePage() {
        dismiss(animated: true, completion: nil)
    }
    
    var buttonClick = false
    
    @objc func showPassword() {
        buttonClick = !buttonClick
        if buttonClick == false {
            toggleButton.setTitle("SHOW", for: .normal)
            passwordField.isSecureTextEntry = true
        } else {
            toggleButton.setTitle("HIDE", for: .normal)
            passwordField.isSecureTextEntry = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        } else {
            submitForm()
        }
        return true
    }
    
    @objc func submitForm() {
        view.endEditing(true)
        
        let email = emailField.text
        let password = passwordField.text
        
        if (email!.isEmpty) {
            emailField.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (password!.isEmpty) {
            passwordField.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (email?.isValidEmail() == false) {
            self.alert(message: StringConstants.invalidEmail, title: StringConstants.error)
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                APIConstants.email: email!,
                APIConstants.password: password!
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
            
            AF.request(APIConstants.baseURL + APIConstants.account + "login", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() { //Check connectivity status
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                let userData = info[APIConstants.data] as! [String: Any]
                                let role = userData[APIConstants.roles] as? [String] ?? [APIConstants.emptyValue]
                                
                                if role.contains(APIConstants.user) == true {
                                    UserDefaults.standard.set(userData[APIConstants.token], forKey: APIConstants.token)
                                    UserDefaults.standard.set(userData[APIConstants.refreshToken], forKey: APIConstants.refreshToken)
                                    UserDefaults.standard.set(userData[APIConstants.email], forKey: APIConstants.emailAddress)
                                    UserDefaults.standard.set(userData[APIConstants.firstname], forKey: APIConstants.firstName)
                                    UserDefaults.standard.set(userData[APIConstants.id], forKey: APIConstants.userId)
                                    
                                    self.switchView(value: true) //Update the current view
                                } else {
                                    self.alert(message: "You are not a registered User", title: StringConstants.unauthorized)
                                }
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
    
    @objc func forgotPassword() {
        presentViewController(viewController: UINavigationController(rootViewController: ForgotPasswordViewController()))
    }
    
    @objc func register() {
        presentViewController(viewController: UINavigationController(rootViewController: RegisterViewController()))
    }
    
    func setEmail() {
        let email = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? APIConstants.emptyValue
        self.emailField.text = email
    }
}
