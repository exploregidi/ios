//
//  RegisterViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    } //Hide keyboard on screen tap
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
    
    let firstNameField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.firstName, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.autocapitalizationType = .words
        field.returnKeyType = .next
        return field
    }()
    
    let lastNameField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.lastName, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.autocapitalizationType = .words
        field.returnKeyType = .next
        return field
    }()
    
    let emailField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .emailAddress
        field.returnKeyType = .next
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let passwordField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.isSecureTextEntry = true
        field.returnKeyType = .go
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let toggleButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("SHOW", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.backgroundColor = ColorConstants.gidiGray.cgColor
        button.layer.cornerRadius = Constants.texFieldHeightOffset
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        button.addTarget(self, action: #selector(showPassword), for: .touchUpInside)
        return button
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {
        view.backgroundColor = .white
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = ColorConstants.gidiGreen
        
        navigationItem.title = "Register an account"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(closePage))
        
        firstNameField.delegate = self
        lastNameField.delegate = self
        emailField.delegate = self
        passwordField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(firstNameField)
        view.addSubview(lastNameField)
        view.addSubview(emailField)
        view.addSubview(passwordField)
        view.addSubview(toggleButton)
        view.addSubview(submitButton)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = firstNameField.anchor(view.safeAreaLayoutGuide.topAnchor, left: emailField.leadingAnchor, topConstant: Constants.topConstraintForTextField, widthConstant: view.frame.width / Constants.textFielsSpacing - (Constants.leadingConstraintForTextField + Constants.textFielsSpacing), heightConstant: Constants.textFieldHeight)
        _ = lastNameField.anchor(view.safeAreaLayoutGuide.topAnchor, right: emailField.trailingAnchor, topConstant: Constants.topConstraintForTextField, widthConstant: view.frame.width / Constants.textFielsSpacing - (Constants.trailingConstraintForTextField + Constants.textFielsSpacing), heightConstant: Constants.textFieldHeight)
        _ = emailField.anchor(lastNameField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = passwordField.anchor(emailField.bottomAnchor, left: emailField.leadingAnchor, right: emailField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, rightConstant: Constants.buttonFormWidth, heightConstant: Constants.textFieldHeight)
        _ = toggleButton.anchor(left: passwordField.trailingAnchor, centerY: passwordField.centerYAnchor, widthConstant: Constants.buttonFormWidth)
        _ = submitButton.anchor(passwordField.bottomAnchor, left: passwordField.leadingAnchor, right: emailField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
    }
    
    @objc func closePage() {
        dismiss(animated: true, completion: nil)
    }
    
    var buttonClick = false
    
    @objc func showPassword() {
        buttonClick = !buttonClick
        if buttonClick == false {
            toggleButton.setTitle("SHOW", for: .normal)
            passwordField.isSecureTextEntry = true
        } else {
            toggleButton.setTitle("HIDE", for: .normal)
            passwordField.isSecureTextEntry = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameField {
            lastNameField.becomeFirstResponder()
        } else if textField == lastNameField {
            emailField.becomeFirstResponder()
        } else if textField == emailField {
            passwordField.becomeFirstResponder()
        } else {
            submitForm()
        }
        return true
    }
    
    @objc func submitForm() {
        view.endEditing(true)
        
        let firstName = firstNameField.text
        let lastName = lastNameField.text
        let email = emailField.text
        let password = passwordField.text
        
        if (firstName!.isEmpty) {
            firstNameField.attributedPlaceholder = NSAttributedString(string: StringConstants.firstName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (lastName!.isEmpty) {
            lastNameField.attributedPlaceholder = NSAttributedString(string: StringConstants.lastName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (email!.isEmpty) {
            emailField.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (password!.isEmpty) {
            passwordField.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (email?.isValidEmail() == false) {
            self.alert(message: StringConstants.invalidEmail, title: StringConstants.error)
        } else if (password?.isValidPassword() == false) {
            self.alert(message: StringConstants.shortPassword, title: StringConstants.error)
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                APIConstants.firstName: firstName!,
                "lastName": lastName!,
                APIConstants.email: email!,
                APIConstants.password: password!
            ] //Parameters needed for registering a user
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
            
            //Alamofire post request
            AF.request(APIConstants.baseURL + APIConstants.account + "register", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() {
                    let info = serializeResponse(data: response.data ?? Data()) //Get raw data from backend
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil { //as? NSNull != NSNull.init()
                                let userData = info[APIConstants.data] as! [String: Any]
                                
                                //Store relevant data into local storage
                                UserDefaults.standard.set(userData[APIConstants.token], forKey: APIConstants.token)
                                UserDefaults.standard.set(userData[APIConstants.refreshToken], forKey: APIConstants.refreshToken)
                                UserDefaults.standard.set(userData[APIConstants.email], forKey: APIConstants.emailAddress)
                                UserDefaults.standard.set(userData[APIConstants.firstname], forKey: APIConstants.firstName)
                                UserDefaults.standard.set(userData[APIConstants.id], forKey: APIConstants.userId)
                                
                                let role = userData[APIConstants.roles] as? [String] ?? [APIConstants.emptyValue]
                                
                                if role.contains(APIConstants.user) == true {
                                    self.switchView(value: true)
                                }
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
}
