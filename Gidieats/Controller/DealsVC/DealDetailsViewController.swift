//
//  DealDetailsViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/30/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class DealDetailsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        getDealDetails()
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.tintColor = .black
        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.purchaseCode)
    }
    
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.frame = self.view.bounds
        view.clipsToBounds = true
        view.contentInsetAdjustmentBehavior = .never
        view.showsVerticalScrollIndicator = false
        view.contentSize = CGSize(width: UIScreen.main.bounds.width, height: Constants.dynamicHeight)
        return view
    }()
    
    let imageContainer = UIView()
    
    let productImages: CustomImageView = {
        let image = CustomImageView()
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        image.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
        return image
    }()
    
    let nameLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.biggerLabelFont)
        label.textAlignment = .left
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let locationLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        return label
    }()
    
    let originalAmount: AuthLabel = {
        let label = AuthLabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: Constants.bodyLabelFont)
        return label
    }()
    
    let discountedAmount: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont.systemFont(ofSize: Constants.biggerLabelFont, weight: .bold)
        label.textColor = ColorConstants.gidiGreen
        return label
    }()
    
    let expiryDateLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont.systemFont(ofSize: Constants.bodyLabelFont)
        label.textColor = .red
        return label
    }()
    
    let purchaseButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Purchase deal", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(buyDeal), for: .touchUpInside)
        return button
    }()
    
    let favoriteButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Add to my favorites", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(addToFavorites), for: .touchUpInside)
        return button
    }()
    
    let detailTagLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: "Kefa", size: Constants.biggerLabelFont)
        label.textAlignment = .left
        label.text = "What's included?"
        return label
    }()
    
    let restaurantTagLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: "Kefa", size: Constants.biggerLabelFont)
        label.textAlignment = .left
        label.text = "Merchant Info"
        return label
    }()
    
    let detailsLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        label.textAlignment = .left
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let restaurantDetails: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        label.textAlignment = .left
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let taxUnderlyingView = UIView()
    
    let taxLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: "Kefa", size: Constants.regularLabelFont)
        label.textColor = .red
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    private func configureUI() {
        view.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
    }
    
    @objc func getQRCode() {
        let qrVc = QRCodeViewController()
        present(qrVc, animated: true, completion: nil)
    }
    
    func getDealDetails() {
        if Connectivity.isConnected() {
            self.activityIndicatorStart()
            self.networkLoaderStart()
            let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
            let dealSlug = UserDefaults.standard.object(forKey: APIConstants.dealSlug) as? String ?? APIConstants.emptyValue
            let urlString = APIConstants.baseURL + "deals/\(dealSlug)"
            guard let urlRequest = URL(string: urlString) else { return }
            var request = URLRequest(url: urlRequest)
            request.httpMethod = APIConstants.get
            request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
            request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                do {
                    let jsonData = try JSONDecoder().decode(DealDetailData.self, from: data!)
                    DispatchQueue.main.async {
                        if let image = jsonData.data.imageUrl {
                            self.productImages.load(urlString: image)
                        }
                        
                        self.nameLabel.text = jsonData.data.name
                        self.locationLabel.text = "\(jsonData.data.restaurant.city.name ?? APIConstants.emptyValue)"
                        
                        var mainPrice = "₦\(jsonData.data.valuePrice ?? APIConstants.emptyValue)"
                        
                        if let modifiedPrice = mainPrice.range(of: ".") {
                            mainPrice.removeSubrange(modifiedPrice.lowerBound ..< mainPrice.endIndex)
                            
                            let originalPrice: NSMutableAttributedString = NSMutableAttributedString(string: mainPrice)
                            originalPrice.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, originalPrice.length))
                            self.originalAmount.attributedText = originalPrice
                        }
                        
                        let discount = (jsonData.data.discountPrice! as NSString).replacingOccurrences(of: ",", with: APIConstants.emptyValue)
                        let finalAmount = (discount as NSString).floatValue
                        let toInt = finalAmount + (finalAmount * 0.1)
                        let finalIntAmount = Int(toInt)
                        self.discountedAmount.text = "₦\(finalIntAmount)"
                        
                        let favorite = jsonData.data.isFavorite
                        
                        if favorite == true {
                            self.favoriteButton.isEnabled = false
                            self.favoriteButton.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
                        }
                        
                        let expiry = jsonData.data.isExpired
                        
                        if expiry == true {
                            self.expiryDateLabel.text = "Expired"
                            self.purchaseButton.isEnabled = false
                            self.favoriteButton.isEnabled = false
                            self.purchaseButton.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
                            self.favoriteButton.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
                            
                            self.taxLabel.text = "This deal expired on \(jsonData.data.expiryDate ?? APIConstants.emptyValue) and cannot be purchased or redeemed anymore."
                            
                        } else {
                            self.expiryDateLabel.text = "\(jsonData.data.expiryDate ?? APIConstants.emptyValue)"
                            
                            let purchaseCode = UserDefaults.standard.object(forKey: APIConstants.purchaseCode) as? String ?? APIConstants.emptyValue
                            
                            if purchaseCode != APIConstants.emptyValue {
                                self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Receipt", style: .plain, target: self, action: #selector(self.getQRCode))
                                self.purchaseButton.isEnabled = false
                                self.purchaseButton.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
                            }
                            
                            self.taxLabel.text = "Please use this deal before \(jsonData.data.expiryDate ?? APIConstants.emptyValue) to avoid forfeit.\nThis deal doesn't include tax."
                        }
                        self.detailsLabel.text = "A ₦\(jsonData.data.valuePrice ?? APIConstants.emptyValue) meal value.\n\(jsonData.data.description ?? APIConstants.emptyValue)"
                        self.restaurantDetails.text = "\(jsonData.data.restaurant.name ?? "This restaurant") is located in \(jsonData.data.restaurant.city.name ?? APIConstants.emptyValue), and you can reach them at \(jsonData.data.restaurant.phoneNumber ?? APIConstants.emptyValue). Currently, this is one of the deals being offered. Please call and ask about the full details of this deal '\(jsonData.data.name ?? APIConstants.emptyValue)' before purchasing it. \(jsonData.data.restaurant.name ?? "This restaurant") thanks you for your patronage as always."
                        
                        let price = (jsonData.data.discountPrice! as NSString).replacingOccurrences(of: ",", with: APIConstants.emptyValue)
                        let formattedPrice = (price as NSString).floatValue
                        let totalPrice = (formattedPrice + (formattedPrice * (0.1)))
                        let totalIntPrice = Int(totalPrice)
                        UserDefaults.standard.set(totalIntPrice, forKey: APIConstants.price)
                        UserDefaults.standard.set(jsonData.data.restaurant.subAccountCode, forKey: APIConstants.subaccountCode)
                        
                        self.imageContainer.viewContainer()
                        self.taxUnderlyingView.translateAll()
                        self.taxUnderlyingView.layer.cornerRadius = Constants.indicatorValue
                        self.taxUnderlyingView.layer.backgroundColor = ColorConstants.gidiGray.withAlphaComponent(Constants.buttonAlpha).cgColor
                        
                        self.view.addSubview(self.scrollView)
                        self.scrollView.addSubview(self.imageContainer)
                        self.imageContainer.addSubview(self.productImages)
                        self.scrollView.addSubview(self.nameLabel)
                        self.scrollView.addSubview(self.locationLabel)
                        self.scrollView.addSubview(self.originalAmount)
                        self.scrollView.addSubview(self.purchaseButton)
                        self.scrollView.addSubview(self.favoriteButton)
                        self.scrollView.addSubview(self.detailTagLabel)
                        self.scrollView.addSubview(self.detailsLabel)
                        self.scrollView.addSubview(self.restaurantTagLabel)
                        self.scrollView.addSubview(self.restaurantDetails)
                        self.scrollView.addSubview(self.taxUnderlyingView)
                        self.taxUnderlyingView.addSubview(self.taxLabel)
                        
                        _ = self.scrollView.anchor(self.view.topAnchor, left: self.view.leadingAnchor, bottom: self.view.bottomAnchor, right: self.view.trailingAnchor)
                        _ = self.imageContainer.anchor(self.scrollView.topAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor)
                        
                        if UIDevice.current.userInterfaceIdiom == .pad {
                            _ = self.imageContainer.anchor(heightConstant: Constants.pictureHeightForPad)
                        } else {
                            _ = self.imageContainer.anchor(heightConstant: Constants.pictureHeight)
                        }
                        
                        _ = self.productImages.anchor(self.imageContainer.topAnchor, left: self.imageContainer.leadingAnchor, bottom: self.imageContainer.bottomAnchor, right: self.imageContainer.trailingAnchor)
                        _ = self.nameLabel.anchor(self.imageContainer.bottomAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels, leftConstant: Constants.sideConstraintForLabel / Constants.viewContentMinDivider, rightConstant: Constants.sideConstraintForLabel / Constants.viewContentMinDivider)
                        _ = self.locationLabel.anchor(self.nameLabel.bottomAnchor, left: self.nameLabel.leadingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        _ = self.originalAmount.anchor(self.locationLabel.bottomAnchor, left: self.nameLabel.leadingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        
                        let labelStack = UIStackView(arrangedSubviews: [self.discountedAmount, self.expiryDateLabel])
                        labelStack.distribution = .equalSpacing
                        
                        self.scrollView.addSubview(labelStack)
                        
                        _ = labelStack.anchor(self.originalAmount.bottomAnchor, left: self.nameLabel.leadingAnchor, right: self.nameLabel.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        _ = self.purchaseButton.anchor(labelStack.bottomAnchor, left: labelStack.leadingAnchor, right: labelStack.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels, heightConstant: Constants.smallButtonSize)
                        _ = self.favoriteButton.anchor(self.purchaseButton.bottomAnchor, left: self.purchaseButton.leadingAnchor, right: self.purchaseButton.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels, heightConstant: Constants.smallButtonSize)
                        _ = self.detailTagLabel.anchor(self.favoriteButton.bottomAnchor, left: self.favoriteButton.leadingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        _ = self.detailsLabel.anchor(self.detailTagLabel.bottomAnchor, left: self.favoriteButton.leadingAnchor, right: self.favoriteButton.trailingAnchor, topConstant: Constants.minConstraintOffset)
                        _ = self.restaurantTagLabel.anchor(self.detailsLabel.bottomAnchor, left: self.detailsLabel.leadingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        _ = self.restaurantDetails.anchor(self.restaurantTagLabel.bottomAnchor, left: self.favoriteButton.leadingAnchor, right: self.favoriteButton.trailingAnchor, topConstant: Constants.minConstraintOffset)
                        _ = self.taxUnderlyingView.anchor(self.restaurantDetails.bottomAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, bottom: self.scrollView.bottomAnchor, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels, heightConstant: Constants.buttonFormWidth)
                        _ = self.taxLabel.anchor(left: self.taxUnderlyingView.leadingAnchor, right: self.taxUnderlyingView.trailingAnchor, centerY: self.taxUnderlyingView.centerYAnchor, leftConstant: Constants.minConstraintOffset, rightConstant: Constants.minConstraintOffset)
                    }
                    self.networkLoaderStop()
                    self.activityIndicatorStop()
                } catch _ {
                    DispatchQueue.main.async {
                        self.purchaseButton.isHidden = true
                        self.favoriteButton.isHidden = true
                        self.taxLabel.isHidden = true
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                        self.navigationController?.navigationBar.tintColor = .black
                    }
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                self.purchaseButton.isHidden = true
                self.favoriteButton.isHidden = true
                self.taxLabel.isHidden = true
                self.internetCheck(text: APIConstants.checkConnection)
                self.networkLoaderStop()
                self.activityIndicatorStop()
                self.navigationController?.navigationBar.tintColor = .black
            }
        }
    }
    
    @objc func buyDeal() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        if token != APIConstants.emptyValue {
            let paymentVc = UINavigationController(rootViewController: PaymentViewController())
            paymentVc.modalPresentationStyle = .fullScreen
            self.navigationController?.present(paymentVc, animated: false, completion: nil)
        } else {
            alert(message: StringConstants.signInWarning, title: StringConstants.unauthorized)
        }
    }
    
    @objc func addToFavorites() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let dealSlug = UserDefaults.standard.object(forKey: APIConstants.dealSlug) as? String ?? APIConstants.emptyValue
        
        if token != APIConstants.emptyValue {
            self.networkLoaderStart()
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                APIConstants.dealSlug: dealSlug
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + token
            ]
            
            AF.request(APIConstants.baseURL + APIConstants.favoriteDeals, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() {
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                self.favoriteButton.isEnabled = false
                                self.favoriteButton.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
                                self.alert(message: "Deal has been added to your favorites", title: StringConstants.success)
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        let response = response.debugDescription
                        if response == APIConstants.unauthorizedResponse {
                            newToken()
                            
                            let time = DispatchTime.now() + Constants.tokenTimer
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                self.addToFavorites()
                            }
                        } else if response.contains(APIConstants.badResponse) {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                        } else {
                            DispatchQueue.main.async {
                                self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                            }
                        }
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.networkLoaderStop()
            }
        } else {
            alert(message: StringConstants.signInWarning, title: StringConstants.unauthorized)
        }
    }
}
