//
//  MyDealsViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/30/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class FavoritesViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchDeals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    var deal: [Deal]? = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    let favoritesRowId = "favoritesRowId"
    
    let textLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGreen
        label.textAlignment = .center
        label.text = "No deals"
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let purchaseDealButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Refresh", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.layer.cornerRadius = Constants.buttonCornerRadius
        button.backgroundColor = ColorConstants.gidiGray
        button.addTarget(self, action: #selector(fetchDeals), for: .touchUpInside)
        return button
    }()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deal?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: favoritesRowId, for: indexPath) as! GenericCollectionViewCell
        cell.deal = deal?[indexPath.item]
        
        if let expiryStatus = deal?[indexPath.item].isExpired {
            var expiryString: Bool?
            
            expiryString = expiryStatus
            
            cell.expiryLabel.text = nil
            cell.expiryLabel.layer.backgroundColor = nil
            
            if expiryStatus == true {
                if expiryStatus == expiryString {
                    DispatchQueue.main.async {
                        cell.expiryLabel.layer.backgroundColor = UIColor.black.cgColor
                        cell.expiryLabel.layer.cornerRadius = Constants.tableHeightSpacingOffest
                        cell.expiryLabel.text = "Expired"
                        cell.addSubview(cell.expiryLabel)
                        _ = cell.expiryLabel.anchor(cell.imageView.topAnchor, right: cell.imageView.trailingAnchor, topConstant: Constants.minConstraintOffset, rightConstant: Constants.minConstraintOffset, widthConstant: Constants.buttonFormWidth, heightConstant: Constants.indicatorCornerRadius)
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightForPad)
        } else {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(deal?[indexPath.item].slug, forKey: APIConstants.dealSlug)
        pushViewController(viewController: DealDetailsViewController())
    }
    
    @objc func fetchDeals() {
        if Connectivity.isConnected() {
            self.activityIndicatorStart()
            self.networkLoaderStart()
            let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
            let urlString = APIConstants.baseURL + APIConstants.favoriteDeals
            guard let urlRequest = URL(string: urlString) else { return }
            var request = URLRequest(url: urlRequest)
            request.httpMethod = APIConstants.get
            request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
            request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                do {
                    let jsonData = try JSONDecoder().decode(DealData.self, from: data!)
                    self.deal = [Deal]()
                    for dictionary in jsonData.data {
                        let section = Deal(slug: dictionary.slug, name: dictionary.name, percentOff: dictionary.percentOff, imageUrl: dictionary.imageUrl, timeCreated: dictionary.timeCreated, expiryDate: dictionary.expiryDate, isExpired: dictionary.isExpired, restaurant: dictionary.restaurant)
                        self.deal?.append(section)
                    }
                    self.networkLoaderStop()
                    self.activityIndicatorStop()
                    
                    DispatchQueue.main.async {
                        if self.deal?.count ?? 0 == 0 {
                            self.addEmptyDealPrompt()
                            self.textLabel.text = "No favorite deals"
                        } else {
                            self.removeEmptyDealPrompt()
                        }
                    }
                } catch _ {
                    let response = response.debugDescription
                    if response.contains(APIConstants.unauthorizedResponse) {
                        newToken()
                        let time = DispatchTime.now() + Constants.tokenTimer
                        DispatchQueue.main.asyncAfter(deadline: time) {
                            self.fetchDeals()
                        }
                    } else if response.contains(APIConstants.badResponse) {
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                        UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                        
                        self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                    } else {
                        DispatchQueue.main.async {
                            self.internetCheck(text: APIConstants.checkConnection)
                            self.networkLoaderStop()
                            self.activityIndicatorStop()
                        }
                    }
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                self.internetCheck(text: APIConstants.checkConnection)
                self.networkLoaderStop()
                self.activityIndicatorStop()
            }
        }
    }
    
    func addEmptyDealPrompt() {
        textLabel.isHidden = false
        purchaseDealButton.isHidden = false
        
        view.addSubview(textLabel)
        view.addSubview(purchaseDealButton)
        _ = textLabel.anchor(left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, centerY: view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = purchaseDealButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    func removeEmptyDealPrompt() {
        textLabel.isHidden = true
        purchaseDealButton.isHidden = true
    }
    
    private func configureUI() {
        collectionView.backgroundColor = .white
        
        navigationController?.navigationBar.shadowImage = UIImage()
        
        navigationItem.title = StringConstants.favorites
        
        collectionView.register(GenericCollectionViewCell.self, forCellWithReuseIdentifier: favoritesRowId)
    }
}

class MyPurchasesViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchDeals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    var purchase: [Purchases]? = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    let myPurchasesRowId = "myPurchasesRowId"
    
    let textLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGreen
        label.textAlignment = .center
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let purchaseDealButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Refresh", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.layer.cornerRadius = Constants.buttonCornerRadius
        button.backgroundColor = ColorConstants.gidiGray
        button.addTarget(self, action: #selector(fetchDeals), for: .touchUpInside)
        return button
    }()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return purchase?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: myPurchasesRowId, for: indexPath) as! GenericCollectionViewCell
        cell.purchase = purchase?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightForPad)
        } else {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(purchase?[indexPath.item].deal.slug, forKey: APIConstants.dealSlug)
        UserDefaults.standard.set(purchase?[indexPath.item].purchaseCode, forKey: APIConstants.purchaseCode)
        pushViewController(viewController: DealDetailsViewController())
    }
    
    @objc func fetchDeals() {
        if Connectivity.isConnected() {
            self.activityIndicatorStart()
            self.networkLoaderStart()
            let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
            let urlString = APIConstants.baseURL + APIConstants.purchases
            guard let urlRequest = URL(string: urlString) else { return }
            var request = URLRequest(url: urlRequest)
            request.httpMethod = APIConstants.get
            request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
            request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                do {
                    let jsonData = try JSONDecoder().decode(PurchasesData.self, from: data!)
                    self.purchase = [Purchases]()
                    for dictionary in jsonData.data {
                        let section = Purchases(purchaseCode: dictionary.purchaseCode, isRedeemed: dictionary.isRedeemed, deal: dictionary.deal, user: dictionary.user)
                        self.purchase?.append(section)
                    }
                    self.networkLoaderStop()
                    self.activityIndicatorStop()
                    
                    DispatchQueue.main.async {
                        if self.purchase?.count ?? 0 == 0 {
                            self.addEmptyDealPrompt()
                            self.textLabel.text = "No purchases"
                        } else {
                            self.removeEmptyDealPrompt()
                        }
                    }
                } catch _ {
                    let response = response.debugDescription
                    if response.contains(APIConstants.unauthorizedResponse) {
                        newToken()
                        
                        let time = DispatchTime.now() + Constants.tokenTimer
                        DispatchQueue.main.asyncAfter(deadline: time) {
                            self.fetchDeals()
                        }
                    } else if response.contains(APIConstants.badResponse) {
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                        UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                        
                        self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                    } else {
                        DispatchQueue.main.async {
                            self.internetCheck(text: APIConstants.checkConnection)
                            self.networkLoaderStop()
                            self.activityIndicatorStop()
                        }
                    }
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                self.internetCheck(text: APIConstants.checkConnection)
                self.networkLoaderStop()
                self.activityIndicatorStop()
            }
        }
    }
    
    func addEmptyDealPrompt() {
        textLabel.isHidden = false
        purchaseDealButton.isHidden = false
        
        view.addSubview(textLabel)
        view.addSubview(purchaseDealButton)
        _ = textLabel.anchor(left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, centerY: view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = purchaseDealButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    func removeEmptyDealPrompt() {
        textLabel.isHidden = true
        purchaseDealButton.isHidden = true
    }
    
    private func configureUI() {
        collectionView.backgroundColor = .white
        
        navigationController?.navigationBar.shadowImage = UIImage()
        
        navigationItem.title = StringConstants.purchases
        
        collectionView.register(GenericCollectionViewCell.self, forCellWithReuseIdentifier: myPurchasesRowId)
    }
}
