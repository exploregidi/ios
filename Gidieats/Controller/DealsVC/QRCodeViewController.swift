//
//  QRCodeViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 6/12/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    let closeButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.close, for: .normal)
        button.setTitleColor(ColorConstants.gidiGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.closeButtonWeight)
        button.addTarget(self, action: #selector(closePage), for: .touchUpInside)
        return button
    }()
    
    let receiptLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: "Kefa", size: Constants.regularLabelFont)
        label.textColor = ColorConstants.gidiGray
        label.numberOfLines = Constants.defaultLabelLine
        label.textAlignment = .left
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let qrCode: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleToFill
        return image
    }()
    
    private func configureUI() {
        view.backgroundColor = .white
        
        view.addSubview(closeButton)
        view.addSubview(qrCode)
        view.addSubview(receiptLabel)
        
        _ = closeButton.anchor(view.safeAreaLayoutGuide.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.minConstraintOffset, rightConstant: Constants.minConstraintOffset)
        _ = qrCode.anchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, widthConstant: Constants.collectionCellHeight, heightConstant: Constants.collectionCellHeight)
        _ = receiptLabel.anchor(qrCode.bottomAnchor, centerX: view.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels)
        
        let code = UserDefaults.standard.object(forKey: APIConstants.purchaseCode) as? String ?? APIConstants.emptyValue
        let image = generateQRCode(from: code)
        qrCode.image = image
        
        let email = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? APIConstants.emptyValue
        
        receiptLabel.text = "Check \(email)\nfor payment receipt"
    }
    
    @objc func closePage() {
        dismiss(animated: false, completion: nil)
    }
}
