//
//  PaymentCardViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 7/13/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Paystack
import Alamofire

class PaymentCardViewController: UIViewController, PSTCKPaymentCardTextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    let imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        image.image = UIImage(named: "pstck")
        image.layer.cornerRadius = Constants.indicatorValue
        image.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
        return image
    }()
    
    let paymentTextField = PSTCKPaymentCardTextField()
    
    let saveButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("SUBMIT", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGray
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submit), for: .touchUpInside)
        return button
    }()
    
    let priceLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        return label
    }()
    
    let warningLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        label.text = "Please do not hit SUBMIT twice"
        label.textColor = .red
        return label
    }()
    
    let price = UserDefaults.standard.integer(forKey: APIConstants.price)
    
    private let email = UserDefaults.standard.string(forKey: APIConstants.emailAddress)
    private let amount = UInt(UserDefaults.standard.integer(forKey: APIConstants.price) * 100)
    private let ref = UUID().uuidString
    private let subaccount = UserDefaults.standard.object(forKey: APIConstants.subaccountCode) as? String ?? APIConstants.emptyValue
    private let currency = "NGN"
    
    func paymentCardTextFieldDidChange(_ textField: PSTCKPaymentCardTextField) {
        saveButton.isEnabled = textField.isValid
        
        if saveButton.isEnabled {
            saveButton.backgroundColor = ColorConstants.gidiGreen
        } else {
            saveButton.backgroundColor = ColorConstants.gidiGray
        }
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        modalTransitionStyle = .crossDissolve
        
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .white
        
        navigationItem.title = "Payment"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(closePage))
        
        paymentTextField.frame = CGRect(x: 15, y: 15, width: self.view.frame.width - 30, height: 45)
        paymentTextField.delegate = self
        
        priceLabel.text = "Total price: ₦" + (price as NSNumber).stringValue
        
        saveButton.isEnabled = false
        
        view.addSubview(imageView)
        view.addSubview(paymentTextField)
        view.addSubview(saveButton)
        view.addSubview(priceLabel)
        view.addSubview(warningLabel)
        
        _ = imageView.anchor(left: paymentTextField.leadingAnchor, bottom: paymentTextField.topAnchor, right: paymentTextField.trailingAnchor, bottomConstant: Constants.standardBottomConstant, heightConstant: Constants.imageHeight)
        _ = paymentTextField.anchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, centerYConstant: Constants.imageHeight)
        _ = saveButton.anchor(paymentTextField.bottomAnchor, left: paymentTextField.leadingAnchor, right: paymentTextField.trailingAnchor, topConstant: Constants.topConstraintForButton, heightConstant: Constants.smallButtonSize)
        _ = priceLabel.anchor(saveButton.bottomAnchor, centerX: saveButton.centerXAnchor, topConstant: Constants.minConstraintOffset)
        _ = warningLabel.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, centerX: view.centerXAnchor, bottomConstant: Constants.standardBottomConstant)
    }
    
    @objc func submit() {
        self.view.endEditing(true)
        
        let cardParams = paymentTextField.cardParams as PSTCKCardParams
        let transactionParams = PSTCKTransactionParams.init()
        
        transactionParams.email = email ?? APIConstants.emptyValue
        transactionParams.amount = amount
        transactionParams.reference = ref
        transactionParams.subaccount = subaccount
        transactionParams.currency = currency
        
        
        PSTCKAPIClient.shared().chargeCard(cardParams, forTransaction: transactionParams, on: self,
                                           didEndWithError: { (error, reference) -> Void in
                                            self.alert(message: error.localizedDescription, title: StringConstants.error)
        },
                                           didRequestValidation: { (reference) -> Void in
                                            _ = APIConstants.emptyValue
        },
                                           didTransactionSuccess: { (reference) -> Void in
                                            let reference = reference
                                            
                                            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                                                        APIConstants.authorization: APIConstants.bearer + "sk_live_77145d502367710b5b13fd4ae5b66ba7298f0aaf"
                                            ]
                                            
                                            AF.request("https://api.paystack.co/transaction/verify/\(reference)", method: .get, parameters: nil, headers: headers).responseJSON { response in
                                                if Connectivity.isConnected() {
                                                    let info = serializeResponse(data: response.data ?? Data())
                                                    switch response.result {
                                                    case .success(_):
                                                        let data = info["data"] as! [String: Any]
                                                        let status = data["status"]
                                                        
                                                        if status as? String == "success" {
                                                            let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
                                                            let dealSlug = UserDefaults.standard.object(forKey: APIConstants.dealSlug) as? String ?? APIConstants.emptyValue
                                                            
                                                            let parameters: [String: Any] = [
                                                                "referenceNumber": reference,
                                                                APIConstants.dealSlug: dealSlug
                                                            ]
                                                            
                                                            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                                                                        APIConstants.authorization: APIConstants.bearer + token
                                                            ]
                                                            
                                                            AF.request(APIConstants.baseURL + "purchases", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                                                                if Connectivity.isConnected() {
                                                                    switch response.result {
                                                                    case .success(_):
                                                                        _ = APIConstants.emptyValue
                                                                    case .failure(let error):
                                                                        _ = error
                                                                    }
                                                                } else {
                                                                    _ = APIConstants.noConnection
                                                                }
                                                            }
                                                        }
                                                    case .failure(let error):
                                                        _ = error
                                                    }
                                                } else {
                                                    _ = APIConstants.noConnection
                                                }
                                            }
                                            self.alertAndDismiss(message: "Payment successful", title: StringConstants.success)
        })
    }
    
    @objc func closePage() {
        dismiss(animated: true, completion: nil)
    }
}
