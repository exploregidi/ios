//
//  PaymentViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 5/21/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

enum ScriptMessageHandler: String {
    case cancelPaymentHandler = "cancelPaymentHandler"
    case transactionResponse = "transactionResponse"
}

enum VerificationState: Int {
    case verifying = 0
    case success
    case failed
}

class PaymentViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token != APIConstants.emptyValue {
            getProfilePicture() //Clever way to refresh expired token
        }
    }
        
    private var webView = WKWebView()
    private var popupWebView: WKWebView?
    private let userContentController = WKUserContentController()
    private let config = WKWebViewConfiguration()
    
    private let paystackKey = "pk_live_908dd8fd441d0cb3a2dbc81b66c1e7e8b327272b"
    private let email = UserDefaults.standard.string(forKey: APIConstants.emailAddress)
    private let amount = UserDefaults.standard.integer(forKey: APIConstants.price) * 100
    private let ref = UUID().uuidString
    private let subaccount = UserDefaults.standard.object(forKey: APIConstants.subaccountCode) as? String ?? APIConstants.emptyValue
    private let currency = "NGN"
    
    private func configureWebView() {
        view.backgroundColor = .white
        
        modalTransitionStyle = .crossDissolve
        
        navigationItem.title = "Payment"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closePage))

        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .white
        
        userContentController.add(self, name: "cancelPaymentHandler")
        userContentController.add(self, name: "transactionResponse")
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true
        
        config.userContentController = userContentController
        config.preferences = preferences
        
        webView = WKWebView(frame: view.bounds, configuration: config)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        view.addSubview(webView)
        
        _ = webView.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        
        let htmlString = """
        
        <!DOCTYPE html>
            <html>
                <head>
                    <!--Let browser know website is optimized for mobile-->
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <script src="https://js.paystack.co/v1/inline.js"></script>
                        <script>
                        
                            function payWithPaystack() {
                                var handler = PaystackPop.setup({
                                key: '\(self.paystackKey)',
                                email: '\(self.email ?? APIConstants.emptyValue)',
                                amount: '\(self.amount)',
                                currency: '\(self.currency)',
                                ref: '\(self.ref)',
                                subaccount: '\(self.subaccount)',
                                callback: function (response) {
                                window.webkit.messageHandlers.transactionResponse.postMessage({"status":"done","ref":response.reference});
                            },
        
                            onClose: function () {
                                alert('Transaction cancelled');
                                window.webkit.messageHandlers.cancelPaymentHandler.postMessage({"status":"user_cancelled"});
                            }
        
                            });
                                handler.openIframe();
                            }
                        
                        </script>
                </head>
                <body onload="payWithPaystack()">
                </body>
            </html>
        
        """
        
        webView.loadHTMLString(htmlString, baseURL: nil)
    }
    
    @objc func closePage() {
        dismiss(animated: true, completion: nil)
    }
    
    func getProfilePicture() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(UserData.self, from: data ?? Data())
                _ = jsonData
            } catch (_) {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + Constants.tokenTimer
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getProfilePicture()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                } else {
                    _ = APIConstants.emptyValue
                }
            }
        }.resume()
    }
}

extension PaymentViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        networkLoaderStart()
        activityIndicatorStart()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        networkLoaderStop()
        activityIndicatorStop()
    }
}

extension PaymentViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        popupWebView = WKWebView(frame: view.bounds, configuration: configuration)
        popupWebView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        popupWebView?.navigationDelegate = self
        popupWebView?.uiDelegate = self
        view.addSubview(popupWebView!)
        return popupWebView!
    }
    
    func webViewDidClose(_ webView: WKWebView) {
        webView.removeFromSuperview()
        popupWebView = nil
    }
}

extension PaymentViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch ScriptMessageHandler(rawValue: message.name)! {
        case .cancelPaymentHandler:
            self.dismiss(animated: true, completion: nil)
            break
        case .transactionResponse:
            guard let dict = message.body as? [String: AnyObject],
                let reference = dict["ref"] as? String else {return}
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + "sk_live_77145d502367710b5b13fd4ae5b66ba7298f0aaf"
            ]
            
            AF.request("https://api.paystack.co/transaction/verify/\(reference)", method: .get, parameters: nil, headers: headers).responseJSON { response in
                if Connectivity.isConnected() {
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        let data = info["data"] as! [String: Any]
                        let status = data["status"]
                        
                        if status as? String == "success" {
                            let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
                            let dealSlug = UserDefaults.standard.object(forKey: APIConstants.dealSlug) as? String ?? APIConstants.emptyValue
                            
                            let parameters: [String: Any] = [
                                "referenceNumber": reference,
                                APIConstants.dealSlug: dealSlug
                            ]
                            
                            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                                        APIConstants.authorization: APIConstants.bearer + token
                            ]
                            
                            AF.request(APIConstants.baseURL + "purchases", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                                if Connectivity.isConnected() {
                                    switch response.result {
                                    case .success(_):
                                        _ = APIConstants.emptyValue
                                    case .failure(let error):
                                        _ = error
                                    }
                                } else {
                                    _ = APIConstants.noConnection
                                }
                            }
                        }
                    case .failure(let error):
                        _ = error
                    }
                } else {
                    _ = APIConstants.noConnection
                }
            }
            self.dismiss(animated: true, completion: nil)
            break
        }
    }
}
