//
//  AccountViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire
import Cloudinary

class AccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.titleTextAttributes = titleColor
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = ColorConstants.gidiGreen
        navigationController?.navigationBar.shadowImage = UIImage()
        
        tabBarController?.tabBar.isHidden = false
        setupAccountButton()
        setupInfo()
    }
    
    let accountContainer = UIView()
    
    private let accountRowId = "accountRowId"
    
    var account = [StringConstants.favorites, StringConstants.purchases, StringConstants.info, StringConstants.deleteAccount]
    
    var imagePicker = UIImagePickerController()
    
    let imageView: CustomImageView = {
        let image = CustomImageView()
        image.layer.borderWidth = Constants.imageBorder
        image.layer.borderColor = UIColor.gray.withAlphaComponent(Constants.imageAlpha).cgColor
        image.layer.cornerRadius = Constants.accountImageCornerRadius
        image.image = UIImage(named: "face")
        image.layer.masksToBounds = true
        image.contentMode = .scaleToFill
        return image
    }()
    
    let nameLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        label.textColor = .black
        return label
    }()
    
    let emailLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        label.textColor = .black
        return label
    }()
    
    let accountTableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.bounces = false
        tableView.isScrollEnabled = false
        return tableView
    }()
    
    let logout: AuthButton = {
        let button = AuthButton()
        button.setTitle("LOG OUT", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(logoutUser), for: .touchUpInside)
        return button
    }()
    
    let login: AuthButton = {
        let button = AuthButton()
        button.setTitle("LOG IN/REGISTER", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(loginUser), for: .touchUpInside)
        return button
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return account.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: accountRowId, for: indexPath)
        cell.textLabel?.text = account[indexPath.row]
        cell.textLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.biggerLabelFont)
        cell.textLabel?.textColor = .black
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.textColor = .black
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let info = account[indexPath.row]
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let layout = UICollectionViewFlowLayout()
        
        if token == APIConstants.emptyValue {
            alert(message: StringConstants.signInWarning, title: StringConstants.unauthorized)
        } else {
            if info == StringConstants.favorites {
                pushViewController(viewController: FavoritesViewController(collectionViewLayout: layout))
            } else if info == StringConstants.purchases {
                pushViewController(viewController: MyPurchasesViewController(collectionViewLayout: layout))
            } else if info == StringConstants.info {
                pushViewController(viewController: ChangeInfoViewController())
            } else {
                pushViewController(viewController: DeleteAccountViewController())
            }
        }
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        tabBarController?.tabBar.shadowImage = UIImage()
        
        navigationItem.title = StringConstants.account
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(pickImage))
        
        accountContainer.viewContainer()
        
        imagePicker.delegate = self
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token != APIConstants.emptyValue {
            getProfilePicture()
        }
        
        accountTableView.delegate = self
        accountTableView.dataSource = self
        accountTableView.separatorStyle = .none
        accountTableView.register(UITableViewCell.self, forCellReuseIdentifier: accountRowId)
        
        view.addSubview(imageView)
        view.addSubview(nameLabel)
        view.addSubview(emailLabel)
        view.addSubview(accountContainer)
        view.addSubview(accountTableView)
        
        _ = imageView.anchor(view.safeAreaLayoutGuide.topAnchor, centerX: view.centerXAnchor, topConstant: Constants.minConstraintOffset, widthConstant: Constants.imageWidth - Constants.tableHeightSpacingOffest, heightConstant: Constants.imageHeight - Constants.tableHeightSpacingOffest).last
        _ = nameLabel.anchor(imageView.bottomAnchor, centerX: imageView.centerXAnchor, topConstant: Constants.minConstraintOffset)
        _ = emailLabel.anchor(nameLabel.bottomAnchor, centerX: view.centerXAnchor, topConstant: Constants.minConstraintOffset)
        _ = accountContainer.anchor(emailLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, heightConstant: CGFloat(account.count * 50))
        _ = accountTableView.anchor(accountContainer.topAnchor, left: accountContainer.leadingAnchor, bottom: accountContainer.bottomAnchor, right: accountContainer.trailingAnchor)
    }
    
    func setupAccountButton() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token == APIConstants.emptyValue {
            view.addSubview(login)
            _ = login.anchor(accountContainer.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, leftConstant: Constants.buttonSideConstraint, rightConstant: Constants.buttonSideConstraint, heightConstant: Constants.smallButtonSize)
        } else {
            view.addSubview(logout)
            _ = logout.anchor(accountContainer.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, leftConstant: Constants.buttonSideConstraint, rightConstant: Constants.buttonSideConstraint, heightConstant: Constants.smallButtonSize)
        }
    }
    
    func setupInfo() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token == APIConstants.emptyValue {
            nameLabel.text = StringConstants.defaultFirstName
            emailLabel.text = StringConstants.defaultEmail
        } else {
            let name = UserDefaults.standard.object(forKey: APIConstants.firstName) as? String ?? StringConstants.defaultFirstName
            let email = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? StringConstants.defaultEmail
            
            nameLabel.text = name.capitalizingFirstLetter()
            emailLabel.text = email.lowercased()
        }
    }
    
    @objc func logoutUser() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let refreshToken = UserDefaults.standard.object(forKey: APIConstants.refreshToken) as? String ?? APIConstants.emptyValue
        
        self.networkLoaderStart()
        self.view.isUserInteractionEnabled = false
        self.activityIndicatorStart()
        
        let parameters: [String: Any] = [
            APIConstants.refreshToken: refreshToken
        ]
        
        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                    APIConstants.authorization: APIConstants.bearer + token
        ]
        
        AF.request(APIConstants.baseURL + APIConstants.account + "logout", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            if Connectivity.isConnected() { //Check connectivity status
                let info = serializeResponse(data: response.data ?? Data())
                switch response.result {
                case .success(_):
                    if info[APIConstants.isSuccess] as? Bool ?? false == true {
                        if info[APIConstants.data] != nil {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.switchView(value: false)
                        }
                    } else {
                        let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                        let message = userData.joined()
                        self.alert(message: message, title: StringConstants.error)
                    }
                case .failure(_):
                    let response = response.debugDescription
                    if response.contains(APIConstants.unauthorizedResponse) {
                        newToken()
                        
                        let time = DispatchTime.now() + Constants.tokenTimer
                        DispatchQueue.main.asyncAfter(deadline: time) {
                            self.logoutUser()
                        }
                    } else if response.contains(APIConstants.badResponse) {
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                        UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                        
                        self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                    } else {
                        DispatchQueue.main.async {
                            self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                        }
                    }
                }
            } else {
                self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
            }
            self.activityIndicatorStop()
            self.view.isUserInteractionEnabled = true
            self.networkLoaderStop()
        }
    }
    
    @objc func loginUser() {
        presentViewController(viewController: UINavigationController(rootViewController: LoginViewController()))
    }
    
    @objc func pickImage() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token == APIConstants.emptyValue {
            alert(message: StringConstants.signInWarning, title: StringConstants.unauthorized)
        } else {
            self.imagePicker.allowsEditing = true
            self.imagePicker.modalPresentationStyle = .overFullScreen
            self.imagePicker.sourceType = .photoLibrary
            
            self.presentViewController(viewController: self.imagePicker)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        activityIndicatorStart()
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            let imgData = image.pngData()            
            let config = CLDConfiguration(cloudName: "explore-gidi", apiKey: "163752141535158")
            let cloudinary = CLDCloudinary(configuration: config)
            cloudinary.createUploader().upload(data: imgData!, uploadPreset: "image-preset") { (response, error) in
                if let error = error {
                    _ = error
                    self.alert(message: "Please upload a smaller sized photo", title: StringConstants.error)
                    self.activityIndicatorStop()
                    self.getProfilePicture()
                } else {
                    if let response = response {
                        let imageUrl = response.secureUrl
                        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
                        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
                        
                        let parameters: [String: Any] = [
                            "profilePictureUrl": imageUrl ?? APIConstants.emptyValue
                        ]
                        
                        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                                    APIConstants.authorization: APIConstants.bearer + token
                        ]
                        
                        AF.request(APIConstants.baseURL + APIConstants.users + userId + "/profile-picture", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                            
                            let info = serializeResponse(data: response.data ?? Data())
                            
                            switch response.result {
                            case .success(_):
                                if info[APIConstants.isSuccess] as? Bool ?? false == true {
                                    if info[APIConstants.data] != nil {
                                        _ = info[APIConstants.data]!
                                        UserDefaults.standard.set(imgData, forKey: APIConstants.savedProfilePicture)
                                        self.imageView.image = image
                                        self.activityIndicatorStop()
                                    }
                                } else {
                                    let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                                    let message = userData.joined()
                                    self.alert(message: message, title: StringConstants.error)
                                }
                            case .failure(_):
                                _ = APIConstants.emptyValue
                                self.activityIndicatorStop()
                            }
                        }
                    }
                }
            }
            dismiss(animated: true, completion: nil)
        }
    }
    
    func getProfilePicture() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let image = UserDefaults.standard.object(forKey: APIConstants.savedProfilePicture) as? Data
                
                if image != nil {
                    DispatchQueue.main.async {
                        self.imageView.image = UIImage(data: image ?? Data())
                    }
                } else {
                    let jsonData = try JSONDecoder().decode(UserData.self, from: data ?? Data())
                    if jsonData.isSuccess == true {
                        self.imageView.load(urlString: jsonData.data.profilePictureUrl ?? APIConstants.emptyValue)
                    }
                }
            } catch (_) {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + Constants.tokenTimer
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getProfilePicture()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                } else {
                    _ = APIConstants.emptyValue
                }
            }
        }.resume()
    }
}
