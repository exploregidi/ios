//
//  CitiesViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class CitiesViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchCities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.titleTextAttributes = titleColor
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = ColorConstants.gidiGreen
        navigationController?.navigationBar.shadowImage = UIImage()
        
        tabBarController?.tabBar.isHidden = false
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token != APIConstants.emptyValue {
            getProfilePicture() //Clever way to refresh expired token
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    var cities: [Cities]? = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    let cellId = "cellId"
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cities?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CitiesCollectionViewCell
        cell.cities = cities?[indexPath.item]
        cell.mainVc = self
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let layout = UICollectionViewFlowLayout()
        let cityDealsVc = CitiesDealsViewController(collectionViewLayout: layout)
        pushViewController(viewController: cityDealsVc)
        UserDefaults.standard.set(cities?[indexPath.item].slug, forKey: APIConstants.citySlug)
        cityDealsVc.navigationItem.title = cities?[indexPath.item].name
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightFrameForPad)
        } else {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightFrame)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func showDealDetails(deal: Deal) {
        let detailVc = DealDetailsViewController()
        pushViewController(viewController: detailVc)
    }
    
    func fetchCities() {
        if Connectivity.isConnected() {
            self.activityIndicatorStart()
            self.networkLoaderStart()
            tabBarController?.tabBar.isUserInteractionEnabled = false
            let urlString = APIConstants.baseURL + "cities"
            guard let url = URL(string: urlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                do {
                    let jsonData = try JSONDecoder().decode(CitiesData.self, from: data!)
                    self.cities = [Cities]()
                    for dictionary in jsonData.data {
                        let section = Cities(deals: dictionary.deals, slug: dictionary.slug, name: dictionary.name)
                        self.cities?.append(section)
                    }
                    
                    DispatchQueue.main.async {
                        self.tabBarController?.tabBar.isUserInteractionEnabled = true
                    }
                    
                    self.networkLoaderStop()
                    self.activityIndicatorStop()
                } catch _ {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                        
                        DispatchQueue.main.async {
                            self.tabBarController?.tabBar.isUserInteractionEnabled = true
                        }
                    }
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                self.internetCheck(text: APIConstants.checkConnection)
                self.networkLoaderStop()
                self.activityIndicatorStop()
                
                DispatchQueue.main.async {
                    self.tabBarController?.tabBar.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    private func configureUI() {
        collectionView.backgroundColor = .white
        
        navigationItem.title = StringConstants.cities
        
        navigationController?.navigationBar.shadowImage = UIImage()
        tabBarController?.tabBar.shadowImage = UIImage()
        
        collectionView?.register(CitiesCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    func getProfilePicture() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(UserData.self, from: data ?? Data())
                _ = jsonData
            } catch (_) {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + Constants.tokenTimer
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getProfilePicture()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                } else {
                    _ = APIConstants.emptyValue
                }
            }
        }.resume()
    }
}

class CitiesDealsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token != APIConstants.emptyValue {
            getProfilePicture() //Clever way to refresh expired token
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchDeals()
    }
    
    var deal: [Deal]? = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    let statusBar = UIView()
    
    let scrollUp: AuthButton = {
        let button = AuthButton()
        button.setTitle("▲", for: .normal)
        button.backgroundColor = ColorConstants.gidiGray
        button.layer.cornerRadius = Constants.scrollCornerRadius
        button.addTarget(self, action: #selector(scrollToTop), for: .touchUpInside)
        return button
    }()
    
    let textLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGreen
        label.textAlignment = .center
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let purchaseDealButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Refresh", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.layer.cornerRadius = Constants.buttonCornerRadius
        button.backgroundColor = ColorConstants.gidiGray
        button.addTarget(self, action: #selector(fetchDeals), for: .touchUpInside)
        return button
    }()
    
    let citiesRowId = "citiesRowId"
    
    var fetchMore = false
    var pageNumber = 0
    
    var dealPageString: String {
        if pageNumber == 0 {
            return "pageNumber=0&"
        } else {
            return "pageNumber=\(pageNumber)&"
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deal?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: citiesRowId, for: indexPath) as! GenericCollectionViewCell
        cell.deal = deal?[indexPath.item]
        
        if let expiryStatus = deal?[indexPath.item].isExpired {
            var expiryString: Bool?
            
            expiryString = expiryStatus
            
            cell.expiryLabel.text = nil
            cell.expiryLabel.layer.backgroundColor = nil
            
            if expiryStatus == true {
                if expiryStatus == expiryString {
                    DispatchQueue.main.async {
                        cell.expiryLabel.layer.backgroundColor = UIColor.black.cgColor
                        cell.expiryLabel.layer.cornerRadius = Constants.tableHeightSpacingOffest
                        cell.expiryLabel.text = "Expired"
                        cell.addSubview(cell.expiryLabel)
                        _ = cell.expiryLabel.anchor(cell.imageView.topAnchor, right: cell.imageView.trailingAnchor, topConstant: Constants.minConstraintOffset, rightConstant: Constants.minConstraintOffset, widthConstant: Constants.buttonFormWidth, heightConstant: Constants.indicatorCornerRadius)
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightForPad)
        } else {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(deal?[indexPath.item].slug, forKey: APIConstants.dealSlug)
        pushViewController(viewController: DealDetailsViewController())
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + 1 >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            if fetchMore {
                navigationItem.leftBarButtonItem = UIBarButtonItem(customView: Activity.indicator)
                
                Activity.indicator.hidesWhenStopped = true
                Activity.indicator.style = UIActivityIndicatorView.Style.gray
                
                pageNumber += 1
                fetchMoreDeals()
            }
        }
    }
    
    @objc func fetchDeals() {
        if Connectivity.isConnected() {
            self.activityIndicatorStart()
            self.networkLoaderStart()
            fetchMore = false
            let citySlug = UserDefaults.standard.object(forKey: APIConstants.citySlug) as? String ?? APIConstants.emptyValue
            let urlString = APIConstants.baseURL + APIConstants.deals + "\(dealPageString)\(APIConstants.itemsPerPage)\(Constants.defaultItems)&citySlug=\(citySlug)"
            guard let url = URL(string: urlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                do {
                    let jsonData = try JSONDecoder().decode(DealData.self, from: data!)
                    for dictionary in jsonData.data {
                        let section = Deal(slug: dictionary.slug, name: dictionary.name, percentOff: dictionary.percentOff, imageUrl: dictionary.imageUrl, timeCreated: dictionary.timeCreated, expiryDate: dictionary.expiryDate, isExpired: dictionary.isExpired, restaurant: dictionary.restaurant)
                        self.deal?.append(section)
                        let count = jsonData.count ?? 0
                        if self.pageNumber == 0 {
                            if (self.pageNumber + (jsonData.size ?? 0)) < count {
                                self.fetchMore = true
                            }
                        }
                    }
                    self.networkLoaderStop()
                    self.activityIndicatorStop()
                    
                    DispatchQueue.main.async {
                        if self.deal?.count ?? 0 == 0 {
                            self.addEmptyDealPrompt()
                            self.textLabel.text = "No deals in \(self.navigationItem.title ?? "this city") currently"
                        } else {
                            self.removeEmptyDealPrompt()
                        }
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                    }
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                self.internetCheck(text: APIConstants.checkConnection)
                self.networkLoaderStop()
                self.activityIndicatorStop()
            }
        }
    }
    
    func fetchMoreDeals() {
        scrollUp.isHidden = false
        self.networkLoaderStart()
        Activity.indicator.startAnimating()
        
        fetchMore = false
        let citySlug = UserDefaults.standard.object(forKey: APIConstants.citySlug) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.deals + "\(dealPageString)\(APIConstants.itemsPerPage)\(Constants.defaultItems)&citySlug=\(citySlug)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(DealData.self, from: data!)
                for dictionary in jsonData.data {
                    let section = Deal(slug: dictionary.slug, name: dictionary.name, percentOff: dictionary.percentOff, imageUrl: dictionary.imageUrl, timeCreated: dictionary.timeCreated, expiryDate: dictionary.expiryDate, isExpired: dictionary.isExpired, restaurant: dictionary.restaurant)
                    self.deal?.append(section)
                    let count = jsonData.count ?? 0
                    if ((self.pageNumber + 1) * Constants.defaultItems) > count {
                        self.fetchMore = false
                    } else {
                        self.fetchMore = true
                    }
                }
                self.networkLoaderStop()
                
                DispatchQueue.main.async {
                    Activity.indicator.stopAnimating()
                }
                
            } catch _ {}
        }.resume()
    }
    
    func addEmptyDealPrompt() {
        textLabel.isHidden = false
        purchaseDealButton.isHidden = false
        
        view.addSubview(textLabel)
        view.addSubview(purchaseDealButton)
        _ = textLabel.anchor(left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, centerY: view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = purchaseDealButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    func removeEmptyDealPrompt() {
        textLabel.isHidden = true
        purchaseDealButton.isHidden = true
    }
    
    private func configureUI() {
        collectionView.backgroundColor = .white
        
        navigationController?.navigationBar.shadowImage = UIImage()
        
        statusBar.frame = UIApplication.shared.statusBarFrame
        
        collectionView.addSubview(scrollUp)
        
        scrollUp.isHidden = true
        _ = scrollUp.anchor(bottom: collectionView.safeAreaLayoutGuide.bottomAnchor, centerX: collectionView.centerXAnchor, bottomConstant: Constants.standardBottomConstant, widthConstant: Constants.indicatorWidth, heightConstant: Constants.indicatorHeight)
        
        collectionView.register(GenericCollectionViewCell.self, forCellWithReuseIdentifier: citiesRowId)
    }
    
    @objc func scrollToTop() {
        scrollUp.isHidden = true
        collectionView.scrollsToTop = true
        collectionView.clipsToBounds = true
        collectionView.setContentOffset(CGPoint(x: 0, y: -((statusBar.frame.size.height) + (navigationController?.navigationBar.frame.size.height ?? 0))), animated: true)
    }
    
    func getProfilePicture() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(UserData.self, from: data ?? Data())
                _ = jsonData
            } catch (_) {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + Constants.tokenTimer
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getProfilePicture()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                } else {
                    _ = APIConstants.emptyValue
                }
            }
        }.resume()
    }
}
