//
//  HomePageViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 2/25/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class HomePageViewController: /*UICollectionViewController*/UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.titleTextAttributes = titleColor
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = ColorConstants.gidiGreen
        navigationController?.navigationBar.shadowImage = UIImage()
        
        tabBarController?.tabBar.isHidden = false
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        if token != APIConstants.emptyValue {
            getProfilePicture() //Clever way to refresh expired token
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchDeals()
    }
    
    var deal: [Deal]? = [] {
        didSet {
            DispatchQueue.main.async {
//                self.collectionView.reloadData()
                self.homeDealsCollectionView.reloadData()
            }
        }
    }
    
    let statusBar =  UIView()
    
    let categoryTextField: UITextField = {
        let textField = UITextField()
        textField.textAlignment = .center
        textField.backgroundColor = UIColor.black.withAlphaComponent(Constants.buttonAlpha)
        textField.borderStyle = .none
        textField.layer.cornerRadius = Constants.indicatorValue
        textField.font = UIFont(name: StringConstants.defaultFont, size: Constants.labelFont)
        textField.text = "FEATURED DEALS"
        textField.textColor = .white
        textField.translateAll()
        return textField
    }()
    
    let homeDealsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translateAll()
        collectionView.backgroundColor = .clear
        collectionView.clipsToBounds = true
        return collectionView
    }()
    
    let dropDown: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "dropdown")
        image.tintColor = .white
        return image
    }()
    
    let scrollUp: AuthButton = {
        let button = AuthButton()
        button.setTitle("▲", for: .normal)
        button.backgroundColor = ColorConstants.gidiGray
        button.layer.cornerRadius = Constants.scrollCornerRadius
        button.addTarget(self, action: #selector(scrollToTop), for: .touchUpInside)
        return button
    }()
    
    let textLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGreen
        label.textAlignment = .center
        label.text = "No deals"
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let purchaseDealButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Refresh", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.layer.cornerRadius = Constants.buttonCornerRadius
        button.backgroundColor = ColorConstants.gidiGray
        button.addTarget(self, action: #selector(fetchDeals), for: .touchUpInside)
        return button
    }()
    
    let homeRowId = "homeRowId"
    
    var categories = ["FEATURED DEALS"]
    
    let categoryPicker = UIPickerView()
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / Constants.pickerViewDivider))
    
    var fetchMore = false
    var pageNumber = 0
    
    var dealPageString: String {
        if pageNumber == 0 {
            return "pageNumber=0&"
        } else {
            return "pageNumber=\(pageNumber)&"
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deal?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeRowId, for: indexPath) as! GenericCollectionViewCell
        cell.deal = deal?[indexPath.item]
        
        if let expiryStatus = deal?[indexPath.item].isExpired {
            var expiryString: Bool?
            
            expiryString = expiryStatus
            
            cell.expiryLabel.text = nil
            cell.expiryLabel.layer.backgroundColor = nil
            
            if expiryStatus == true {
                if expiryStatus == expiryString {
                    DispatchQueue.main.async {
                        cell.expiryLabel.layer.backgroundColor = UIColor.black.cgColor
                        cell.expiryLabel.layer.cornerRadius = Constants.tableHeightSpacingOffest
                        cell.expiryLabel.text = "Expired"
                        cell.addSubview(cell.expiryLabel)
                        _ = cell.expiryLabel.anchor(cell.imageView.topAnchor, right: cell.imageView.trailingAnchor, topConstant: Constants.minConstraintOffset, rightConstant: Constants.minConstraintOffset, widthConstant: Constants.buttonFormWidth, heightConstant: Constants.indicatorCornerRadius)
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightForPad)
        } else {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(deal?[indexPath.item].slug, forKey: APIConstants.dealSlug)
        pushViewController(viewController: DealDetailsViewController())
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = categories[row]
        return row
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + 1 >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            if fetchMore {
                navigationItem.leftBarButtonItem = UIBarButtonItem(customView: Activity.indicator)
                
                Activity.indicator.hidesWhenStopped = true
                Activity.indicator.style = UIActivityIndicatorView.Style.gray
                Activity.indicator.tintColor = .white
                
                pageNumber += 1
                fetchMoreDeals()
            }
        }
    }
    
    @objc func fetchDeals() {
        if Connectivity.isConnected() {
            self.activityIndicatorStart()
            self.networkLoaderStart()
            tabBarController?.tabBar.isUserInteractionEnabled = false
            fetchMore = false
            let urlString = APIConstants.baseURL + APIConstants.deals + "\(dealPageString)\(APIConstants.itemsPerPage)\(Constants.defaultItems)"
            guard let url = URL(string: urlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                do {
                    let jsonData = try JSONDecoder().decode(DealData.self, from: data!)
                    for dictionary in jsonData.data {
                        let section = Deal(slug: dictionary.slug, name: dictionary.name, percentOff: dictionary.percentOff, imageUrl: dictionary.imageUrl, timeCreated: dictionary.timeCreated, expiryDate: dictionary.expiryDate, isExpired: dictionary.isExpired, restaurant: dictionary.restaurant)
                        self.deal?.append(section)
                        let count = jsonData.count ?? 0
                        if self.pageNumber == 0 {
                            if (self.pageNumber + (jsonData.size ?? 0)) < count {
                                self.fetchMore = true
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.tabBarController?.tabBar.isUserInteractionEnabled = true
                    }
                    
                    self.networkLoaderStop()
                    self.activityIndicatorStop()
                    
                    DispatchQueue.main.async {
                        if self.deal?.count ?? 0 == 0 {
                            self.addEmptyDealPrompt()
                        } else {
                            self.removeEmptyDealPrompt()
                        }
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                        
                        DispatchQueue.main.async {
                            self.tabBarController?.tabBar.isUserInteractionEnabled = true
                        }
                    }
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                self.internetCheck(text: APIConstants.checkConnection)
                self.networkLoaderStop()
                self.activityIndicatorStop()
                
                DispatchQueue.main.async {
                    self.tabBarController?.tabBar.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    func fetchMoreDeals() {
        scrollUp.isHidden = false
        self.networkLoaderStart()
        Activity.indicator.startAnimating()
        
        fetchMore = false
        let urlString = APIConstants.baseURL + APIConstants.deals + "\(dealPageString)\(APIConstants.itemsPerPage)\(Constants.defaultItems)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(DealData.self, from: data!)
                for dictionary in jsonData.data {
                    let section = Deal(slug: dictionary.slug, name: dictionary.name, percentOff: dictionary.percentOff, imageUrl: dictionary.imageUrl, timeCreated: dictionary.timeCreated, expiryDate: dictionary.expiryDate, isExpired: dictionary.isExpired, restaurant: dictionary.restaurant)
                    self.deal?.append(section)
                    let count = jsonData.count ?? 0
                    //Let's do some math so we don't go out of range. Since page number starts from 0, we need to use (pageNumber + 1) to detect our last page. We need to hardcode our size for the last page to the number of items per page so we can multiply and know when we have hit our last content on the last page
                    if ((self.pageNumber + 1) * Constants.defaultItems) > count {
                        self.fetchMore = false
                    } else {
                        self.fetchMore = true
                    }
                }
                self.networkLoaderStop()
                
                DispatchQueue.main.async {
                    Activity.indicator.stopAnimating()
                }
                
            } catch _ {}
        }.resume()
    }
    
    func addEmptyDealPrompt() {
        textLabel.isHidden = false
        purchaseDealButton.isHidden = false
        
        view.addSubview(textLabel)
        view.addSubview(purchaseDealButton)
        _ = textLabel.anchor(left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, centerY: view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = purchaseDealButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    func removeEmptyDealPrompt() {
        textLabel.isHidden = true
        purchaseDealButton.isHidden = true
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        statusBar.frame = UIApplication.shared.statusBarFrame
        statusBar.backgroundColor = .clear
        UIApplication.shared.keyWindow?.addSubview(statusBar)
                
        tabBarController?.tabBar.shadowImage = UIImage()
        tabBarController?.tabBar.backgroundImage = UIImage()
        tabBarController?.tabBar.backgroundColor = .white
        
        navigationItem.titleView = categoryTextField
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadApp))
        
        homeDealsCollectionView.delegate = self
        homeDealsCollectionView.dataSource = self
        homeDealsCollectionView.register(GenericCollectionViewCell.self, forCellWithReuseIdentifier: homeRowId)
        
        view.addSubview(homeDealsCollectionView)
        view.addSubview(scrollUp)
        
        _ = homeDealsCollectionView.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        
        scrollUp.isHidden = true
        _ = scrollUp.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, centerX: view.centerXAnchor, bottomConstant: Constants.standardBottomConstant, widthConstant: Constants.indicatorWidth, heightConstant: Constants.indicatorHeight)
        
        categoryTextField.delegate = self
        categoryTextField.sizeToFit()
        categoryTextField.tintColor = .clear
        categoryTextField.font = UIFont(name: StringConstants.defaultFont, size: Constants.midLabelFont)
        _ = categoryTextField.anchor(widthConstant: Constants.textFieldWidth, heightConstant: Constants.textFieldHeight - (Constants.texFieldHeightOffset))
        
        categoryTextField.addSubview(dropDown)
        _ = dropDown.anchor(right: categoryTextField.trailingAnchor, centerY: categoryTextField.centerYAnchor, rightConstant: Constants.dropDownTrail)
    }
    
    @objc func reloadApp() {
        switchView(value: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showCategoryPicker()
        return true
    }
    
    func showCategoryPicker() {
        categoryPicker.dataSource = self
        categoryPicker.delegate = self
        categoryPicker.showsSelectionIndicator = true
        categoryPicker.sizeToFit()
        
        categoryTextField.inputView = categoryPicker
        
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        toolbar.translateAll()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneCategory))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelCategory))
        
        doneButton.tintColor = ColorConstants.gidiGreen
        cancelButton.tintColor = .black
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        categoryTextField.inputAccessoryView = toolbar
        
        self.view.isUserInteractionEnabled = false
    }
    
    @objc func doneCategory() {
        if categories.count == 0 {
            categoryTextField.text = APIConstants.emptyValue
        } else {
            categoryTextField.text = categories[categoryPicker.selectedRow(inComponent: 0)]
        }
        
        self.view.isUserInteractionEnabled = true
        categoryTextField.resignFirstResponder()
    }
    
    @objc func cancelCategory() {
        self.view.isUserInteractionEnabled = true
        categoryTextField.resignFirstResponder()
    }
    
    @objc func scrollToTop() {
        scrollUp.isHidden = true
        homeDealsCollectionView.scrollsToTop = true
        homeDealsCollectionView.clipsToBounds = true
        homeDealsCollectionView.setContentOffset(CGPoint(x: 0, y: -((statusBar.frame.size.height) + (navigationController?.navigationBar.frame.size.height ?? 0))), animated: true)
    }
    
    func getProfilePicture() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(UserData.self, from: data ?? Data())
                _ = jsonData
            } catch (_) {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + Constants.tokenTimer
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getProfilePicture()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                } else {
                    _ = APIConstants.emptyValue
                }
            }
        }.resume()
    }
}
