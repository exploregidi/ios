//
//  SupportViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import SafariServices

class SupportViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.titleTextAttributes = titleColor
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = ColorConstants.gidiGreen
        navigationController?.navigationBar.shadowImage = UIImage()
        
        tabBarController?.tabBar.isHidden = false
    }
    
    let supportContainer = UIView()
    
    private let supportRowId = "supportRowId"
    
    var support = [StringConstants.about, StringConstants.policy, StringConstants.terms, StringConstants.contact]
    
    let supportTableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.bounces = false
        tableView.showsVerticalScrollIndicator = false
        tableView.isScrollEnabled = false
        return tableView
    }()
    
    let rateButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("RATE GIDI EATS", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(rate), for: .touchUpInside)
        return button
    }()
    
    let socialButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Follow us on Instagram", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = #colorLiteral(red: 0.6722204685, green: 0.08677836508, blue: 0.3940520585, alpha: 1)
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(goToInsta), for: .touchUpInside)
        return button
    }()
    
    let version: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGray
        label.font = UIFont.systemFont(ofSize: Constants.midLabelFont, weight: .regular)
        return label
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return support.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: supportRowId, for: indexPath)
        cell.textLabel?.text = support[indexPath.row]
        cell.textLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.biggerLabelFont)
        cell.textLabel?.textColor = .black
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.accessoryType = .disclosureIndicator
        cell.tintColor = .black
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let info = support[indexPath.row]
        
        if info == StringConstants.about {
            showWebPage(link: "https://www.gidieats.com/about")
        } else if info == StringConstants.policy {
            showWebPage(link: "https://www.gidieats.com/privacy-policy")
        } else if info == StringConstants.terms {
            showWebPage(link: "https://www.gidieats.com/terms-conditions")
        } else {
            showWebPage(link: "https://www.gidieats.com/contact-us")
        }
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        navigationItem.title = StringConstants.support
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareApp))
        
        tabBarController?.tabBar.shadowImage = UIImage()
        
        supportContainer.viewContainer()
        
        supportTableView.delegate = self
        supportTableView.dataSource = self
        supportTableView.separatorStyle = .none
        supportTableView.register(UITableViewCell.self, forCellReuseIdentifier: supportRowId)
        
        view.addSubview(supportContainer)
        view.addSubview(supportTableView)
        view.addSubview(rateButton)
        view.addSubview(socialButton)
        view.addSubview(version)
        
        _ = supportContainer.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, widthConstant: view.frame.width, heightConstant: CGFloat(support.count * 50))
        _ = supportTableView.anchor(supportContainer.topAnchor, left: supportContainer.leadingAnchor, bottom: supportContainer.bottomAnchor, right: supportContainer.trailingAnchor)
        _ = rateButton.anchor(supportContainer.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, leftConstant: Constants.buttonSideConstraint, rightConstant: Constants.buttonSideConstraint, heightConstant: Constants.smallButtonSize)
        _ = socialButton.anchor(rateButton.bottomAnchor, left: rateButton.leadingAnchor, right: rateButton.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
        _ = version.anchor(socialButton.bottomAnchor, centerX: view.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels)
        
        version.text = "v\(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") ?? "0.0.0")"
    }
    
    @objc func rate() {
        let urlString = StringConstants.appPageUrl + StringConstants.appID
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    @objc func goToInsta() {
        let urlString = "https://instagram.com/gidieats_"
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    @objc func shareApp() {
        let url = URL(string: StringConstants.appPageUrl + StringConstants.appID)!
        let vc = UIActivityViewController(activityItems: [StringConstants.share, url], applicationActivities: nil)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        } else {
            vc.popoverPresentationController?.sourceView = self.view
        }
        presentViewController(viewController: vc)
    }
    
    func showWebPage(link: String) {
        if let url = URL(string: link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = false
            config.barCollapsingEnabled = true

            let vc = SFSafariViewController(url: url, configuration: config)
            vc.delegate = self
            vc.dismissButtonStyle = .close
            vc.preferredControlTintColor = ColorConstants.gidiGreen
            
            presentViewController(viewController: vc)
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
       dismiss(animated: true)
    }
}
