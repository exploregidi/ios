//
//  TabController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/7/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

//This sets up the tab bar for our whole app look
class TabController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserDefaults.standard.set(true, forKey: StringConstants.status)
        configureUI()
    }
    
    private func configureUI() {
        let tabController = UITabBarController()
        let layout = UICollectionViewFlowLayout()
        
        let homeVc = UINavigationController()//(rootViewController: HomePageViewController(collectionViewLayout: layout))
        homeVc.tabBarItem.title = "Home"
        homeVc.tabBarItem.image = UIImage(named: "home")
        
        let citiesVc = UINavigationController(rootViewController: CitiesViewController(collectionViewLayout: layout))
        citiesVc.tabBarItem.title = StringConstants.cities
        citiesVc.tabBarItem.image = UIImage(named: "city")
        
        let accountVc = UINavigationController(rootViewController: AccountViewController())
        accountVc.tabBarItem.title = StringConstants.account
        accountVc.tabBarItem.image = UIImage(named: "account")
        
        let supportVc = UINavigationController(rootViewController: SupportViewController())
        supportVc.tabBarItem.title = StringConstants.support
        supportVc.tabBarItem.image = UIImage(named: "support")
        
        tabController.viewControllers = [homeVc, citiesVc, accountVc, supportVc]
        tabController.tabBar.tintColor = ColorConstants.gidiGreen
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = tabController
    }
}
