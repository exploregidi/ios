//
//  AlertExtension.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 8/17/19.
//  Copyright © 2019 Gidieats. All rights reserved.
//

import UIKit

//Helper for easier alert setup
extension UIViewController {
    func alert(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: nil)
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndDismiss(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.dismiss(animated: true, completion: nil)})
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndExit(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.switchView(value: false)})
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndDismissToRoot(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)})
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndPop(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    struct Activity {
        static let indicator = UIActivityIndicatorView(frame: CGRect(x: Constants.indicatorValue, y: Constants.indicatorValue, width: Constants.indicatorWidth, height: Constants.indicatorHeight))
        static let overlay = UIView()
    }
    
    func activityIndicatorStart() {
        DispatchQueue.main.async {
            Activity.overlay.isHidden = false
            Activity.overlay.translateAll()
            Activity.overlay.layer.backgroundColor = UIColor.white.cgColor
            Activity.overlay.layer.cornerRadius = Constants.buttonCorner
            
            self.view.addSubview(Activity.overlay)
            _ = Activity.overlay.anchor(centerX: self.view.centerXAnchor, centerY: self.view.centerYAnchor, widthConstant: Constants.frameInset, heightConstant: Constants.frameInset)
            
            Activity.indicator.hidesWhenStopped = true
            Activity.indicator.style = UIActivityIndicatorView.Style.gray
            Activity.indicator.startAnimating()
            
            Activity.overlay.addSubview(Activity.indicator)
            _ = Activity.indicator.anchor(centerX: Activity.overlay.centerXAnchor, centerY: Activity.overlay.centerYAnchor)
        }
    }
    
    func activityIndicatorStop() {
        DispatchQueue.main.async {
            Activity.overlay.isHidden = true
            Activity.indicator.stopAnimating()
        }
    }
    
    func networkLoaderStart() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func networkLoaderStop() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func pushViewController(viewController: UIViewController) {
        let vc = viewController
        self.navigationController?.pushViewController(vc, animated: true)
        return
    }
    
    func presentViewController(viewController: UIViewController) {
        let vc = viewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        return
    }
    
    func switchView(value: Bool, key: String = StringConstants.status) {
        UserDefaults.standard.set(value, forKey: key)
        ViewSwitcher.updateRootVC()
    }
    
    func internetCheck(text: String) {
        let textLabel: AuthLabel = {
            let label = AuthLabel()
            label.textColor = ColorConstants.gidiGreen
            label.textAlignment = .center
            label.text = text
            label.numberOfLines = Constants.defaultLabelLine
            return label
        }()
        
        let refreshButton: AuthButton = {
            let button = AuthButton()
            button.setTitle("Reload", for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
            button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
            button.layer.cornerRadius = Constants.buttonCornerRadius
            button.backgroundColor = ColorConstants.gidiGray
            button.addTarget(self, action: #selector(refresh), for: .touchUpInside)
            return button
        }()
        
        self.view.addSubview(textLabel)
        self.view.addSubview(refreshButton)
        _ = textLabel.anchor(left: self.view.safeAreaLayoutGuide.leadingAnchor, right: self.view.safeAreaLayoutGuide.trailingAnchor, centerY: self.view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = refreshButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    @objc func refresh() {
        switchView(value: true)
    }
    
    struct LoadingView {
        static let textLabel: AuthLabel = {
            let label = AuthLabel()
            label.textAlignment = .center
            label.textColor = .white
            label.text = "Loading..."
            label.layer.backgroundColor = UIColor.lightGray.withAlphaComponent(Constants.labelBackgroundAlpha).cgColor
            label.layer.cornerRadius = Constants.buttonDivider
            label.font = UIFont.systemFont(ofSize: Constants.normalLabelFont, weight: .regular)
            label.numberOfLines = Constants.defaultLabelLine
            return label
        }()
    }
    
    func showLoadingView() {
        DispatchQueue.main.async {
            LoadingView.textLabel.isHidden = false
            self.view.addSubview(LoadingView.textLabel)
            _ = LoadingView.textLabel.anchor(centerX: self.view.centerXAnchor, centerY: self.view.centerYAnchor, widthConstant: Constants.labelBorderWidth, heightConstant: Constants.labelBorderHeight)
        }
    }
    
    func hideLoadingView() {
        DispatchQueue.main.async {
            LoadingView.textLabel.isHidden = true
        }
    }
}
