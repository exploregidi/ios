//
//  UIButton.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 8/17/19.
//  Copyright © 2019 Gidieats. All rights reserved.
//

import UIKit

//Generic button confuguration
class AuthButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    private func setupButton() {
        translateAll()
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: Constants.buttonFont, weight:UIFont.Weight.medium)
        self.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
    }
}
