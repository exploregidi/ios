//
//  UILabel.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 8/19/19.
//  Copyright © 2019 Gidieats. All rights reserved.
//

import UIKit

//Generic label configuration
class AuthLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLabel()
    }
    
    private func setupLabel() {
        translateAll()
        self.textColor = .black
        self.textAlignment = .center
        self.font = UIFont.systemFont(ofSize: Constants.labelFont, weight: UIFont.Weight.regular)
        self.font = UIFont(name: StringConstants.defaultFont, size: Constants.labelFont)
    }
}
