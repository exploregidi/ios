//
//  UITextField.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 8/17/19.
//  Copyright © 2019 Gidieats. All rights reserved.
//

import UIKit

//Generic textfield configuration
class AuthTextField: UITextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTextField()
    }
    
    private func setupTextField() {
        translateAll()
        
        let bottomLine = UIView()
        
        borderStyle = .none
        bottomLine.translateAll()
        bottomLine.backgroundColor = .gray
        
        self.addSubview(bottomLine)
        _ = bottomLine.anchor(self.bottomAnchor, left: self.leadingAnchor, right: self.trailingAnchor, heightConstant: Constants.dynamicHeight)
        
        self.backgroundColor = .clear
        self.layer.delegate = self
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self.textColor = .black
        self.tintColor = ColorConstants.gidiGreen
        self.textAlignment = .left
        self.font = UIFont.systemFont(ofSize: Constants.fontForTextField, weight: .regular)
        self.font = UIFont(name: StringConstants.defaultFont, size: Constants.fontForTextField)
    }
}
