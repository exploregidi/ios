//
//  UnitTestExtension.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 8/17/19.
//  Copyright © 2019 Gidieats. All rights reserved.
//

import UIKit

//Helper for setting up unit tests
extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let validPassword = self.count >= Constants.minPassword
        return (validPassword)
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
}

extension UserDefaults {
    private static var index = 0
    static func createCleanForTest(label: StaticString = #file) -> UserDefaults {
        index += 1
        let suiteName = "UnitTest-UserDefaults-\(label)-\(index)"
        UserDefaults().removePersistentDomain(forName: suiteName)
        return UserDefaults(suiteName: suiteName)!
    }
}

class CustomImageView: UIImageView {
    
    var imageCache = NSCache<AnyObject, AnyObject>()
    
    var imageUrlString: String?
    
    func load(urlString: String) {
        imageCache = NSCache()
        
        imageUrlString = urlString
        
        image = nil
        
        let url = URL(string: urlString)
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url ?? URL(fileURLWithPath: urlString), completionHandler:
            { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                
                DispatchQueue.global().async {
                    if let imageToCache = UIImage(data: data!) {
                        if self.imageUrlString == urlString {
                            DispatchQueue.main.async {
                                self.image = imageToCache
                                self.imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                            }
                        }
                    }
                }
        }).resume()
    }
}
