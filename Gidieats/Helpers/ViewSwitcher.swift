//
//  ViewSwitcher.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 8/26/19.
//  Copyright © 2019 Gidieats. All rights reserved.
//

import UIKit

//Helps with easier view switching
class ViewSwitcher {
    static func updateRootVC() {
        let status = UserDefaults.standard.bool(forKey: StringConstants.status)
        
        var rootVc: UIViewController?
        
        if status == true {
            rootVc = TabController()
        } else {
            rootVc = UINavigationController(rootViewController: WelcomePageViewController())
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVc
    }
}
