//
//  JSONDecoding.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 4/14/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import Foundation

struct Deal: Decodable {
    var slug: String?
    var name: String?
    var percentOff: String?
    var imageUrl: String?
    var timeCreated: String?
    var expiryDate: String?
    var isExpired: Bool?
    var restaurant: RestaurantDetail
}

struct DealData: Decodable {
    var data: [Deal]
    var page: Int?
    var size: Int?
    var count: Int?
    var status: Int?
    var isSuccess: Bool?
}

struct DealDetail: Decodable {
    var description: String?
    var discountPrice: String?
    var valuePrice: String?
    var isFavorite: Bool?
    var imageUrl: String?
    var slug: String?
    var name: String?
    var percentOff: String?
    var expiryDate: String?
    var isExpired: Bool?
    var restaurant: RestaurantDetail
}

struct RestaurantDetail: Decodable {
    var email: String?
    var phoneNumber: String?
    var subAccountCode: String?
    var city: City
    var slug: String?
    var name: String?
    var imageUrl: String?
}

struct City: Decodable {
    var slug: String?
    var name: String?
}

struct DealDetailData: Decodable {
    var data: DealDetail
    var status: Int?
    var isSuccess: Bool?
}

struct Cities: Decodable {
    var deals: [Deal]
    var slug: String?
    var name: String?
}

struct CitiesData: Decodable {
    var data: [Cities]
    var page: Int?
    var size: Int?
    var count: Int?
    var status: Int?
    var isSuccess: Bool?
}

struct User: Decodable {
    var id: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var phoneNumber: String?
    var profilePictureUrl: String?
}

struct UserData: Decodable {
    var data: User
    var status: Int?
    var isSuccess: Bool?
}

struct Purchases: Decodable {
    var purchaseCode: String?
    var isRedeemed: Bool?
    var deal: Deal
    var user: JSONNull
}

struct PurchasesData: Decodable {
    var data: [Purchases]
    var page: Int?
    var size: Int?
    var count: Int?
    var status: Int?
    var isSuccess: Bool?
}

class JSONNull: Codable, Hashable {
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public func hash(into hasher: inout Hasher) {}
}
