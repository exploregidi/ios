//
//  NetworkReachability.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/29/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnected() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
