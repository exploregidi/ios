//
//  RefreshToken.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 5/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import Foundation
import Alamofire

public func refreshUserToken(refreshToken: String, userId: String) {
    let parameters: [String: Any] = [
        APIConstants.refreshToken: refreshToken,
        APIConstants.userId: userId
    ]
    
    let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
    
    AF.request(APIConstants.baseURL + APIConstants.account + "refresh-token", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
        
        if Connectivity.isConnected() { //Check connectivity status
            let info = serializeResponse(data: response.data ?? Data())
            
            switch response.result {
            case .success(_):
                if info[APIConstants.isSuccess] as? Bool ?? false == true {
                    if info[APIConstants.data] != nil {
                        let userData = info[APIConstants.data] as! [String: Any]
                        UserDefaults.standard.set(userData[APIConstants.token], forKey: APIConstants.token)
                        UserDefaults.standard.set(userData[APIConstants.refreshToken], forKey: APIConstants.refreshToken)
                        print("Done...")
                    }
                } else {
                    let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                    let message = userData.joined()
                    _ = message
                    print(message)
                }
            case .failure(_):
                _ = APIConstants.emptyValue
                print("Refresh Token Error...")
            }
        } else {
            _ = APIConstants.emptyValue
        }
    }
}

public func newToken() {
    let refreshToken = UserDefaults.standard.object(forKey: APIConstants.refreshToken) as? String ?? APIConstants.emptyValue
    let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
    refreshUserToken(refreshToken: refreshToken, userId: userId)
}
