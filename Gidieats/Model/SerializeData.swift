//
//  SerializeData.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/16/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import Foundation
import UIKit

//Serializing data from backend
public func serializeResponse(data: Data) -> [String: Any] {
    do {
        let result = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        return result ?? [:]
    } catch _ {
        return [:]
    }
}

public func generateQRCode(from string: String) -> UIImage? {
    let data = string.data(using: String.Encoding.ascii)

    if let filter = CIFilter(name: "CIQRCodeGenerator") {
        filter.setValue(data, forKey: "inputMessage")
        let transform = CGAffineTransform(scaleX: 3, y: 3)

        if let output = filter.outputImage?.transformed(by: transform) {
            return UIImage(ciImage: output)
        }
    }
    return nil
}
