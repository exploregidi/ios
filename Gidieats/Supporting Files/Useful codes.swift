//
//  Useful codes.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 7/12/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

/*
 
 override func viewDidLayoutSubviews() {
 super.viewDidLayoutSubviews()
 collectionView.contentInsetAdjustmentBehavior = .never
 collectionView.contentInset.top = collectionView.safeAreaInsets.top
 }
 
 override func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
 let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
 
 if (actualPosition.y > 0) {
 tabBarController?.tabBar.isHidden = false
 } else {
 tabBarController?.tabBar.isHidden = true
 }
 }
 
 if let modifiedDiscountText = discount.range(of: ".") {
 discount.removeSubrange(modifiedDiscountText.lowerBound ..< discount.endIndex)
 let finalAmount = (discount as NSString).floatValue
 let toInt = finalAmount + (finalAmount * 0.1)
 let finalIntAmount = Int(toInt)
 self.discountedAmount.text = "\(finalIntAmount)"
 }
 
 func applicationWillResignActive(_ application: UIApplication) {
 let application = UIApplication.shared
 BackgroundTask.run(application: application) { BackgroundTask in
 BackgroundTask.end()
 }
 }
 
 class BackgroundTask {
 private let application: UIApplication
 private var identifier = UIBackgroundTaskIdentifier.invalid
 
 init(application: UIApplication) {
 self.application = application
 }
 
 class func run(application: UIApplication, handler: (BackgroundTask) -> ()) {
 // NOTE: The handler must call end() when it is done
 
 let backgroundTask = BackgroundTask(application: application)
 backgroundTask.begin()
 handler(backgroundTask)
 }
 
 func begin() {
 self.identifier = application.beginBackgroundTask {
 self.end()
 }
 }
 
 func end() {
 if (identifier != UIBackgroundTaskIdentifier.invalid) {
 application.endBackgroundTask(identifier)
 }
 
 identifier = UIBackgroundTaskIdentifier.invalid
 }
 }
 
 override func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
 let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
 
 if (actualPosition.y > 0) {
 tabBarController?.tabBar.isHidden = false
 } else {
 tabBarController?.tabBar.isHidden = true
 }
 }
 
 override func viewDidLayoutSubviews() {
 super.viewDidLayoutSubviews()
 collectionView.contentInsetAdjustmentBehavior = .never
 collectionView.contentInset.top = collectionView.safeAreaInsets.top
 }
 
 */
