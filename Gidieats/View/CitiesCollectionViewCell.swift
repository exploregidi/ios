//
//  CitiesCollectionViewCell.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 5/17/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class CitiesCollectionViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    var cities: Cities? {
        didSet {
            if let name = cities?.name {
                headerLabel.text = name
            }
            cityDeals.reloadData()
        }
    }
    
    var mainVc: CitiesViewController?
    
    private let cellId = "cellId"
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Constants.regularLabelFont, weight: .medium)
        label.textColor = .black
        label.translateAll()
        return label
    }()
    
    let buttonLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .semibold)
        label.textColor = ColorConstants.gidiGray
        label.text = "View All"
        label.translateAll()
        return label
    }()
    
    let cityDeals: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.bounces = false
        collectionView.reloadData()
        collectionView.translateAll()
        return collectionView
    }()
    
    let separatorView = UIView()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let deals = cities?.deals.count {
            if deals >= 2 {
                return 2
            }
        }
        return cities?.deals.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DealsCell
        cell.deals = cities?.deals[indexPath.item]
        
        if let expiryStatus = cities?.deals[indexPath.item].isExpired {
            var expiryString: Bool?
            
            expiryString = expiryStatus
            
            cell.expiryLabel.text = nil
            
            if expiryStatus == true {
                if expiryStatus == expiryString {
                    DispatchQueue.main.async {
                        cell.expiryLabel.text = "Expired"
                        cell.expiryLabel.textColor = .red
                        cell.contentContainer.addSubview(cell.expiryLabel)
                        _ = cell.expiryLabel.anchor(cell.percentOffLabel.bottomAnchor, left: cell.nameLabel.leadingAnchor, topConstant: Constants.minConstraintOffset, widthConstant: Constants.buttonFormWidth, heightConstant: Constants.indicatorCornerRadius)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    cell.expiryLabel.text = "Live"
                    cell.expiryLabel.textColor = ColorConstants.gidiGreen
                    cell.contentContainer.addSubview(cell.expiryLabel)
                    _ = cell.expiryLabel.anchor(cell.percentOffLabel.bottomAnchor, left: cell.nameLabel.leadingAnchor, topConstant: Constants.minConstraintOffset, widthConstant: Constants.buttonFormWidth, heightConstant: Constants.indicatorCornerRadius)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((frame.width - Constants.minConstraintOffset) / Constants.widthDivider), height: frame.height - Constants.frameInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.75
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let deals = cities?.deals[indexPath.item] {
            UserDefaults.standard.set(deals.slug, forKey: APIConstants.dealSlug)
            mainVc?.showDealDetails(deal: deals)
        }
    }
    
    private func configureUI() {
        backgroundColor = .white
        
        cityDeals.dataSource = self
        cityDeals.delegate = self
        cityDeals.register(DealsCell.self, forCellWithReuseIdentifier: cellId)
        
        separatorView.translateAll()
        separatorView.backgroundColor = ColorConstants.gidiGray.withAlphaComponent(Constants.buttonAlpha)
        
        addSubview(headerLabel)
        addSubview(buttonLabel)
        addSubview(cityDeals)
        addSubview(separatorView)
        
        _ = headerLabel.anchor(topAnchor, left: safeAreaLayoutGuide.leadingAnchor, topConstant: Constants.minConstraintOffset, leftConstant: Constants.minConstraintOffset)
        _ = buttonLabel.anchor(right: safeAreaLayoutGuide.trailingAnchor, centerY: headerLabel.centerYAnchor, rightConstant: Constants.minConstraintOffset)
        _ = cityDeals.anchor(headerLabel.bottomAnchor, left: safeAreaLayoutGuide.leadingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: safeAreaLayoutGuide.trailingAnchor, leftConstant: Constants.collectionSideSpacing, rightConstant: Constants.collectionSideSpacing)
        addConstraintsWithFormat(format: "H:|[v0]|", views: separatorView)
        addConstraintsWithFormat(format: "V:[v0(0)]|", views: separatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DealsCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    var deals: Deal? {
        didSet {
            if let name = deals?.name {
                nameLabel.text = name
            }
            if let percentOff = deals?.percentOff {
                let percent = (percentOff as NSString).integerValue
                percentOffLabel.text = "\(percent)% off"
            }
            if let image = deals?.imageUrl {
                imageView.load(urlString: image)
            }
        }
    }
    
    let contentContainer = UIView()
    
    let imageView: CustomImageView = {
        let img = CustomImageView()
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        img.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
        img.translateAll()
        return img
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Constants.midLabelFont, weight: .medium)
        label.translateAll()
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.textColor = .black
        return label
    }()
    
    let percentOffLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        label.textColor = .darkGray
        label.translateAll()
        return label
    }()
    
    let expiryLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        label.textAlignment = .left
        return label
    }()
    
    private func configureUI() {
        backgroundColor = .white
        
        contentContainer.translateAll()
        contentContainer.backgroundColor = .white
        contentContainer.layer.cornerRadius = Constants.buttonCornerRadius
        contentContainer.layer.borderColor = ColorConstants.gidiGray.cgColor
        contentContainer.layer.borderWidth = Constants.imageAlpha
        contentContainer.layer.masksToBounds = true
        
        addSubview(contentContainer)
        contentContainer.addSubview(imageView)
        contentContainer.addSubview(nameLabel)
        contentContainer.addSubview(percentOffLabel)
        
        _ = contentContainer.anchor(safeAreaLayoutGuide.topAnchor, left: safeAreaLayoutGuide.leadingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: safeAreaLayoutGuide.trailingAnchor, leftConstant: Constants.textFielsSpacing, rightConstant: Constants.textFielsSpacing)
        _ = imageView.anchor(contentContainer.topAnchor, left: contentContainer.leadingAnchor, right: contentContainer.trailingAnchor, heightConstant: frame.height - Constants.buttonFormWidth)
        _ = nameLabel.anchor(imageView.bottomAnchor, left: imageView.leadingAnchor, right: imageView.trailingAnchor, topConstant: Constants.minConstraintOffset, leftConstant: Constants.minConstraintOffset)
        _ = percentOffLabel.anchor(nameLabel.bottomAnchor, left: imageView.leadingAnchor, topConstant: Constants.minConstraintOffset, leftConstant: Constants.minConstraintOffset)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
