//
//  GenericCollectionViewCell.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/12/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class GenericCollectionViewCell: UICollectionViewCell, UICollectionViewDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    var deal: Deal? {
        didSet {
            if let dealName = deal?.name {
                if let restaurantName = deal?.restaurant.name {
                    if let location = deal?.restaurant.city.name {
                        if let percentOff = deal?.percentOff {
                            let percent = (percentOff as NSString).integerValue
                            nameLabel.text = "\(percent)% off \(dealName) at \(restaurantName), \(location)"
                        }
                    }
                }
            }
            if let image = deal?.imageUrl {
                imageView.load(urlString: image)
            }
        }
    }
    
    var purchase: Purchases? {
        didSet {
            if let dealName = purchase?.deal.name {
                if let restaurantName = purchase?.deal.restaurant.name {
                    if let percentOff = purchase?.deal.percentOff {
                        let percent = (percentOff as NSString).integerValue
                        nameLabel.text = "\(percent)% off \(dealName) at \(restaurantName)"
                    }
                }
            }
            if let image = purchase?.deal.imageUrl {
                imageView.load(urlString: image)
            }
        }
    }
    
    let imageView: CustomImageView = {
        let image = CustomImageView()
        image.contentMode = .scaleAspectFill
        image.layer.masksToBounds = true
        image.translateAll()
        image.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
        return image
    }()
    
    let nameLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        label.lineBreakMode = .byTruncatingTail
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = Constants.labelNumberOfLines
        label.layer.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.labelBackgroundAlpha).cgColor
        return label
    }()
    
    let expiryLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        label.textColor = .white
        return label
    }()
    
    let separatorView = UIView()
    
    private func configureUI() {
        backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        separatorView.translateAll()
        separatorView.backgroundColor = .white
        
        addSubview(imageView)
        addSubview(nameLabel)
        addSubview(separatorView)
        
        _ = imageView.anchor(topAnchor, left: leadingAnchor, right: trailingAnchor, heightConstant: frame.height - Constants.buttonDivider)
        _ = nameLabel.anchor(left: imageView.leadingAnchor, bottom: imageView.bottomAnchor, right: imageView.trailingAnchor, heightConstant: Constants.labelBackgroundHeight)
        addConstraintsWithFormat(format: "H:|[v0]|", views: separatorView)
        addConstraintsWithFormat(format: "V:[v0(4)]|", views: separatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
